<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

define('__ROOT__', dirname(dirname(__FILE__)));
include( __ROOT__.'/Session.php');
// error_log('sess group ---> ' . $_SESSION['usergroup']);
// DB table to use
$table = 'group_list_'. $_SESSION['usergroup'];

// Table's primary key
$primaryKey = 'def_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'def_id', 			'dt' => 0 ),
	array( 'db' => 'first_name',  		'dt' => 1 ),
	array( 'db' => 'middle_initial',  	'dt' => 2 ),
	array( 'db' => 'last_name',  		'dt' => 3 ),
	array( 'db' => 'phone_number',   	'dt' => 4 ),
	array( 'db' => 'nric',     			'dt' => 5 ),
	array( 'db' => 'race',     			'dt' => 6 ),
	array( 'db' => 'address1',     		'dt' => 7 ),
	array( 'db' => 'postal_code',     	'dt' => 8 ),
	array( 'db' => 'modify_date',     	'dt' => 9 )
);

$ip_server = $_SERVER['SERVER_ADDR']; 
// SQL server connection information
$sql_details = array(
	'user' => 'client',
	'pass' => 'vicidialnow',
	'db'   => 'asterisk',
	'host' => $ip_server,
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// $encoded = json_encode($value, DEFINED('JSON_INVALID_UTF8_IGNORE') ? JSON_INVALID_UTF8_IGNORE : 0);

require( __ROOT__.'/ssp.class.php' );

$json = json_encode(
	SSP::defaultlead( $_GET, $sql_details, $table, $primaryKey, $columns), DEFINED('JSON_INVALID_UTF8_IGNORE') ? JSON_INVALID_UTF8_IGNORE : 0 
	);

// error_log('lists lists lists--->> '.$lists);
if ($json)
    echo $json;
else
	error_log('error json --->>> ' .  json_last_error_msg());
	


