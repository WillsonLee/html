<?php
 /**
 * @file 		goGetLeads.php
 * @brief 		API for Getting Leads
 * @copyright   Copyright (c) 2018 GOautodial Inc.
 * @author		Demian Lizandro A. Biscocho
 * @author		Warren Ipac Briones
 * @author     	Alexander Abenoja
 * @author     	Chris Lomuntad
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

    include_once ("goAPI.php");

	$campaigns 											= allowed_campaigns($log_group, $goDB, $astDB);
	$search 											= $astDB->escape($_REQUEST['search']);
	$goVarLimit 										= $astDB->escape($_REQUEST["goVarLimit"]);
	$asc 												= $astDB->escape($_REQUEST["asc"]);
	$desc 												= $astDB->escape($_REQUEST["desc"]);
	$offset 											= $astDB->escape($_REQUEST["offset"]);
	$lead_id 											= $astDB->escape($_REQUEST["lead_id"]);
	$list_ids											= array();

	// ERROR CHECKING 
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} elseif (empty($campaigns) || is_null($campaigns)) {
		$err_msg 										= error_handle("40001");
        $apiresults 									= array(
			"code" 											=> "40001",
			"result" 										=> $err_msg
		);
    } else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level,user_group");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		$usergroup										= $fresults["user_group"];
        
        $tenant                                         = ($userlevel < 9 && $usergroup !== "ADMIN") ? 1 : 0;
		
		if ($goapiaccess > 0 && $userlevel > 7) {
            if ($tenant) {
                $astDB->where("user_group", $usergroup);
            } else {
                if (strtoupper($usergroup) != 'ADMIN') {
                    if ($user_level > 8) {
                        $astDB->where("user_group", $usergroup);
                    }
                }
            }
            $SELECTQuery 							= $astDB->get("vicidial_campaigns", NULL, "campaign_id");
            $array_camp = array();
            foreach($SELECTQuery as $camp_val){
                $array_camp[] 						= $camp_val["campaign_id"];
            }
            
			if (is_array($array_camp)) {
				$listids								= $astDB
				->where("campaign_id", $array_camp, "IN")
				->get("vicidial_lists", NULL, "list_id");
			}
	
			if ($astDB->count > 0){
				foreach ($listids as $listid) {
					$list_ids[]							= $listid["list_id"];
				}
			}
			error_log('search ---> ' . $search);
			if (!empty($search)) {
				$astDB->where("phone_number", "%$search%", "LIKE");
				$astDB->orWhere("first_name", "%$search%", "LIKE");
				$astDB->orWhere("lead_id", "%$search%", "LIKE");
				$astDB->orWhere("nric", "%$search%", "LIKE");
				$astDB->orWhere("race", "%$search%", "LIKE");
				$astDB->orWhere("address1", "%$search%", "LIKE");
				$astDB->orWhere("postal_code", "%$search%", "LIKE");
			}

			
            if (count($list_ids) < 1) {
                $list_ids = array("-1");
            }
            
			$astDB->where("list_id", $list_ids, "IN");
			error_log('asc ---> ' . $asc);
			error_log('desc ---> ' . $desc);
			error_log('goVarLimit ---> ' . $goVarLimit);
			error_log('offset ---> ' . $offset);
			if($asc != ''){
				$astDB->orderBy($asc, "ASC");
			}else{
				$astDB->orderBy($desc, "DESC");
			}
			/*
				this ->orderby() ->get() is call CodeIgniter 		
			*/
			//$astDB->offset($offset);
			$fresultsv 								= $astDB->get("vicidial_list",[$offset, $goVarLimit], "lead_id");
			error_log('sql -->> ' . $astDB->getLastQuery());
			$log_id 								= log_action($goDB, 'ADD', $log_user, $ip_address, "Add Lead Export Activity", $log_group, $astDB->getLastQuery());

			$SQLdate 								= date("Y-m-d H:i:s");
			// error_log('fresultsv -->> ' . $fresultsv);
			// error_log('count fresultsv -->> ' . count($fresultsv));
			// error_log('fresultsv[0] -->> ' . $fresultsv[0]);

			error_log('fresultsv count ---> ' . count($fresultsv));
			foreach ($fresultsv as $fresults) {
				$insertData 						= array(
					'lead_id' 							=> $fresults['lead_id'],
					'log_user' 							=> $log_user,
					'date' 								=> $SQLdate
				);
				
				$addResult 							= $astDB->insert('vicidial_lead_export_log', $insertData);	
			}
			$apiresults 							= array(
				"result" 								=> "success"
			);
					
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}
	
?>
