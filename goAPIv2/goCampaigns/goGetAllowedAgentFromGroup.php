<?php
/**
 * @file 		goGetUserGroupInfo.php
 * @brief 		API to get specific User Group Details
 * @copyright 	Copyright (c) 2018 GOautodial Inc.
 * @author		Demian Lizandro A. Biscocho
 * @author     	Alexander Jim H. Abenoja
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

	include_once ("goAPI.php");
		
    // POST or GET Variables
    $campaign_id 										= $astDB->escape($_REQUEST['campaign_id']);
    
	// Error Checking
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		
		if ($goapiaccess > 0 && $userlevel > 7) {	
			// set tenant value to 1 if tenant - saves on calling the checkIfTenantf function
			// every time we need to filter out requests
			$tenant										=  (checkIfTenant ($log_group, $goDB)) ? 1 : 0;
			/*
			if ($tenant) {
				$astDB->where("user_group", $log_group);
			} else {
				if (strtoupper($log_group) != "ADMIN") {
					if ($userlevel > 8) {
						$astDB->where("user_group", $log_group);
					}
				}	
			}	
			*/
			$cols										= array(
				"campaign_id", 
				"user_id"
			);
			
			//error_log(' test campaign_id - > '. $campaign_id);
			$query 										= $astDB
				->where("campaign_id", $campaign_id)
				->orderBy("user_id", "ASC")
				->get("vicidial_agent_allowed_campaign");

			//error_log(' test db count - > '. $astDB->count);

				if ($astDB->count > 0) {			
					foreach ($query as $fresults) {
						$dataCampaignID[] 						= $fresults['campaign_id'];		
						$dataUserID[] 							= $fresults['user_id'];
																			
					}
			// error_log(' test campaign_id[0] - > '. $dataCampaignID[0]);
			// error_log(' test campaign_id[1] - > '. $dataCampaignID[1]);
			// error_log(' test $dataUserID[0] - > '. $dataUserID[0]);
			// error_log(' test $dataUserID[1] - > '. $dataUserID[1]);
						
					$apiresults 							= array(
						"result" 								=> "success", 
						"user_id" 								=> $dataUserID,
						"campaign_id" 							=> $dataCampaignID
					);		
					

				} else {
				$apiresults 							= array(
					"result" 								=> "Error: campaign id doesn't exist."
				);
			}
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}
	
?>
