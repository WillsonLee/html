<?php
 /**
 * @file 		goGetLeads.php
 * @brief 		API for Getting Leads
 * @copyright   Copyright (c) 2018 GOautodial Inc.
 * @author		Demian Lizandro A. Biscocho
 * @author		Warren Ipac Briones
 * @author     	Alexander Abenoja
 * @author     	Chris Lomuntad
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

    include_once ("goAPI.php");

	$campaigns 											= allowed_campaigns($log_group, $goDB, $astDB);
	$search 											= $astDB->escape($_REQUEST['search']);
	$goVarLimit 										= $astDB->escape($_REQUEST["goVarLimit"]);
	$limit 												= 10000;	
	$list_ids											= array();

	// ERROR CHECKING 
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} elseif (empty($campaigns) || is_null($campaigns)) {
		$err_msg 										= error_handle("40001");
        $apiresults 									= array(
			"code" 											=> "40001",
			"result" 										=> $err_msg
		);
    } else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level,user_group");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		$usergroup										= $fresults["user_group"];
        
        $tenant                                         = ($userlevel < 9 && $usergroup !== "ADMIN") ? 1 : 0;
		
		if ($goapiaccess > 0 && $userlevel > 7) {
            if ($tenant) {
                $astDB->where("user_group", $usergroup);
            } else {
                if (strtoupper($usergroup) != 'ADMIN') {
                    if ($user_level > 8) {
                        $astDB->where("user_group", $usergroup);
                    }
                }
            }
            $SELECTQuery 							= $astDB->get("vicidial_campaigns", NULL, "campaign_id");
            $array_camp = array();
            foreach($SELECTQuery as $camp_val){
                $array_camp[] 						= $camp_val["campaign_id"];
            }
            
			if (is_array($array_camp)) {
				$listids								= $astDB
				->where("campaign_id", $array_camp, "IN")
				->get("vicidial_lists", NULL, "list_id");
			}
	
			if ($astDB->count > 0){
				foreach ($listids as $listid) {
					$list_ids[]							= $listid["list_id"];
				}
			}
			
			// if (!empty($search)) {
			// 	$astDB->where("phone_number", "%$search%", "LIKE");
			// 	$astDB->orWhere("first_name", "$search%", "LIKE");
			// 	$astDB->orWhere("lead_id", "$search%", "LIKE");
			// 	$astDB->orWhere("nric", "$search%", "LIKE");
			// 	$astDB->orWhere("race", "$search%", "LIKE");
			// 	$astDB->orWhere("address1", "$search%", "LIKE");
			// 	$astDB->orWhere("postal_code", "$search%", "LIKE");
			// }


			// if ($goVarLimit > 0) {
			// 	$limit 									= $goVarLimit;
			// }
			
            // if (count($list_ids) < 1) {
            //     $list_ids = array("-1");
            // }
            
            // $astDB->where("list_id", $list_ids, "IN");
			// $fresultsv 									= $astDB->get("vicidial_list", $limit, "*");
			
			
			// $datago 									= array();
			// $data 										= array();
			
			// foreach ($fresultsv as $fresults) {
			// 	$dataLeadid[] 						= $fresults['lead_id'];
			// 	$dataListid[] 						= $fresults['list_id'];
			// 	$dataFirstName[] 					= $fresults['first_name'];
			// 	$dataPhoneNumber[] 					= $fresults['phone_number'];
			// 	$dataNRIC[] 						= $fresults['nric'];
			// 	$dataRace[] 						= $fresults['race'];
			// 	$dataAddress1[] 					= $fresults['address1'];
			// 	$dataPostal_code[] 					= $fresults['postal_code'];
				
			// 	array_push($datago, $fresults);
		
			// }

			// $apiresults 							= array(
			// 	"result" 								=> "success", 
			// 	"lead_id" 								=> $dataLeadid, 
			// 	"list_id" 								=> $dataListid, 
			// 	"first_name" 							=> $dataFirstName, 
			// 	"phone_number" 							=> $dataPhoneNumber, 
			// 	"nric" 									=> $dataNRIC, 
			// 	"race" 									=> $dataRace, 
			// 	"staaddress1tus" 						=> $dataAddress1, 
			// 	"postal_code" 							=> $dataPostal_code, 
			// 	"search"								=> $search,
			// 	"data" 									=> $datago
			// );
			
			$apiresults 							= array(
				"result" 								=> "success", 
				"list_id" 								=> $list_ids
			);
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}
	
?>
