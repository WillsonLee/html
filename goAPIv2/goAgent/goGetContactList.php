<?php
 /**
 * @file 		goGetContactList.php
 * @brief 		API for Agent UI
 * @copyright 	Copyright (C) GOautodial Inc.
 * @author     	Chris Lomuntad <chris@goautodial.com>
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$agent = get_settings('user', $astDB, $goUser);


if (isset($_GET['goCampaign'])) { $Campaign = $astDB->escape($_GET['goCampaign']); }
    else if (isset($_POST['goCampaign'])) { $Campaign = $astDB->escape($_POST['goCampaign']); }

if (isset($_GET['goLimit'])) { $limit = $astDB->escape($_GET['goLimit']); }
    else if (isset($_POST['goLimit'])) { $limit = $astDB->escape($_POST['goLimit']); }

if (isset($_GET['goLeadSearchMethod'])) { $agent_lead_search_method = $astDB->escape($_GET['goLeadSearchMethod']); }
    else if (isset($_POST['goLeadSearchMethod'])) { $agent_lead_search_method = $astDB->escape($_POST['goLeadSearchMethod']); }

if (isset($_GET['goIsLoggedIn'])) { $is_logged_in = $astDB->escape($_GET['goIsLoggedIn']); }
    else if (isset($_POST['goIsLoggedIn'])) { $is_logged_in = $astDB->escape($_POST['goIsLoggedIn']); }

if (isset($_GET['goSearchString'])) { $search_string = $astDB->escape($_GET['goSearchString']); }
    else if (isset($_POST['goSearchString'])) { $search_string = $astDB->escape($_POST['goSearchString']); }

if (!isset($limit) || !is_numeric($limit)) {
    $limit = 10000;
}

// $astDB->where('user_group', $agent->user_group);
// $rslt = $astDB->getOne('vicidial_user_groups', 'allowed_campaigns');
// $allowed_campaigns = trim(preg_replace("/\ -$/", "", $rslt['allowed_campaigns']));

// //close search for old allowed campaign
// if (!$is_logged_in) {
//     if (!preg_match("/ALL-CAMPAIGNS/", $allowed_campaigns)) {
//         $camp_array = preg_split("/[\s,]+/", $allowed_campaigns);
//         $astDB->where('campaign_id', $camp_array, 'in');
//     }
// } else {
//     if (preg_match("/CAMPLISTS/", $agent_lead_search_method)) {
//         $astDB->where('campaign_id', $campaign);
//     } else {
//         if (!preg_match("/ALL-CAMPAIGNS/", $allowed_campaigns)) {
//             $camp_array = preg_split("/[\s,]+/", $allowed_campaigns);
//             $astDB->where('campaign_id', $camp_array, 'in');
//         }
//     }
// }
// //use agent allowed campaign
$x = 0;
do{
    $query = $astDB->rawQuery("SELECT * FROM `vicidial_callbacks` WHERE lead_id IN (SELECT lead_id FROM `vicidial_callbacks` WHERE 1 and status !='INACTIVE' and user_group='FCgroup' group by lead_id HAVING count(*)>1) and status !='INACTIVE' ORDER BY `vicidial_callbacks`.`lead_id` ASC , callback_time ASC , callback_id ASC");
    $leadidss = array();
    $callidss = array();
    foreach ($query as $row) {
        $leadidss[] = $row['lead_id'];
        $callidss[] = $row['callback_id'];
    }
    // error_log('start lead ids ====> ' . count($leadidss));

    if (count($leadidss) > 1 ) {
        if($leadidss[0] == $leadidss[1]){
            error_log('matched lead ids ====> ' . $leadidss[0] . ' & ' . $leadidss[1]);
            error_log('matched callidss ====> ' . $callidss[0] . ' & ' . $callidss[1]);

            $updateData = array(
            'status' => 'INACTIVE',
            );
            $astDB->where('callback_id', $callidss[0]);
            $rslt = $astDB->update('vicidial_callbacks', $updateData);
        }
    }
    //double check
    $query = $astDB->rawQuery("SELECT * FROM `vicidial_callbacks` WHERE lead_id in(SELECT lead_id FROM `vicidial_callbacks` WHERE 1 and status !='INACTIVE' and user_group='FCgroup' group by lead_id HAVING count(*)>1) and status !='INACTIVE' ORDER BY `vicidial_callbacks`.`lead_id` ASC");
    $leadidss = array();
    $callidss = array();
    foreach ($query as $row) {
        $leadidss[] = $row['lead_id'];
        $callidss[] = $row['callback_id'];
    }
    // error_log('end leadidss = ' . count($callidss)); 
    $x++;
    // error_log('counter x = ' . $x); 
}while(count($callidss)>1);


$query = $astDB->rawQuery("SELECT * FROM vicidial_agent_allowed_campaign WHERE user_group = 'ADMIN' OR user_group = '$agent->user_group' AND user_id = '0' OR user_id = '$agent->user_id' GROUP by campaign_id");
$camps = array();
foreach ($query as $row) {
    $camps[] = $row['campaign_id'];
}

$astDB->where('campaign_id', $campaign);
$astDB->where('active', 'Y');
$rslt = $astDB->get('vicidial_lists', null, 'list_id');

$list_ids = array();
foreach ($rslt as $val) {
    $list_ids[] = $val['list_id'];
}


if (count($list_ids) > 0 ) {
    // $astDB->where('vl.list_id', $list_ids, 'in');
    // $astDB->where('vl.status', array('DNC', 'DNCL'), 'not in');
    // if (strlen($search_string) >= 3) {
    //     $astDB->where("CONCAT(first_name,' ',last_name)", "%$search_string%", 'like');
    //     $astDB->orWhere('phone_number', "%$search_string%", 'like');
    //     $astDB->orWhere('lead_id', "%$search_string%", 'like');
    //     $astDB->orWhere('nric', "%$search_string%", 'like');
    //     $astDB->orWhere('comments', "%$search_string%", 'like');
    //     $astDB->orWhere('title', "%$search_string% ", 'like');
    // }
    // // $astDB->join('vicidial_lists vls', 'vls.list_id=vl.list_id', 'left');
    // $rslt = $astDB->get('vicidial_list vl', $limit, 'lead_id,title,first_name,middle_initial,last_name,phone_number,last_local_call_time,nric,status,comments,phone_code');
    // $lastQuery = $astDB->getLastQuery();
    $List = implode(', ', $list_ids);
    
    $sqlstatement  = "SELECT  vl.lead_id,vl.title,vl.first_name,vl.middle_initial,vl.last_name,vl.phone_number,vl.last_local_call_time,vc.callback_time,vl.nric,vl.status,vl.comments,vl.phone_code ";
    $sqlstatement .= "FROM vicidial_list vl ";
    $sqlstatement .= "left join (select lead_id, callback_time from vicidial_callbacks where status !='INACTIVE') vc on vl.lead_id = vc.lead_id ";
    $sqlstatement .= "WHERE   vl.list_id in ( ". $List ." ) ";
    $sqlstatement .= "AND vl.status not in ( 'DNC', 'DNCL' ) ";
    if (strlen($search_string) >= 3) {
        $sqlstatement .= "AND (CONCAT(vl.first_name,' ',vl.last_name) like '%".$search_string."%'  OR vl.phone_number like '%".$search_string."%'  OR vl.lead_id like '%".$search_string."%'  OR vl.nric like '%".$search_string."%'  OR vl.comments like '%".$search_string."%'  OR vl.title like '%".$search_string."%' )";
    }
    // $sqlstatement .= "LIMIT 1000 ";
    //error_log('sql statment = > '. $sqlstatement);
    $rslt = $astDB->rawQuery($sqlstatement);
    //error_log('sql222 -----> '. $astDB->getLastQuery());
    $leads = array();
    foreach ($rslt as $lead) {
        $leads[] = $lead;
    }

    $APIResult = array( 'result' => 'success', 'leads' => $leads, 'lastQuery' => $lastQuery );
} else {
   // $APIResult = array( 'result' => 'error', 'message' => 'No leads found.' );
}
?>