<?php

/**
 * @file 		crm.php
 * @brief 		Manage leads and contacts
 * @copyright 	Copyright (c) 202 GOautodial Inc. 
 * @author		Demian Lizandro A. Biscocho
 * @author     	Alexander Jim H. Abenoja
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

require_once('./php/UIHandler.php');
require_once('./php/APIHandler.php');
require_once('./php/CRMDefaults.php');
require_once('./php/LanguageHandler.php');
include('./php/Session.php');

$ui = \creamy\UIHandler::getInstance();
$api = \creamy\APIHandler::getInstance();
$lh = \creamy\LanguageHandler::getInstance();
$user = \creamy\CreamyUser::currentUser();

//proper user redirects
if ($user->getUserRole() != CRM_DEFAULTS_USER_ROLE_ADMIN) {
	if ($user->getUserRole() == CRM_DEFAULTS_USER_ROLE_AGENT) {
		header("location: agent.php");
	}
}
?>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php $lh->translateText('portal_title'); ?> - <?php $lh->translateText("lead_log"); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<?php
	print $ui->standardizedThemeCSS();
	print $ui->creamyThemeCSS();
	print $ui->dataTablesTheme();
	?>

	<!-- Datetime picker -->
	<link rel="stylesheet" href="js/dashboard/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

	<!-- Date Picker -->
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/moment.js"></script>
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<!-- BEGIN willson crm -->
	<link rel="stylesheet" href="DataTables/datatables.min.css">
	<!-- END willson crm -->

	<!-- CHOSEN-->
	<link rel="stylesheet" src="js/dashboard/chosen_v1.2.0/chosen.min.css">
</head>
<?php print $ui->creamyBody(); ?>
<div class="wrapper">
	<!-- header logo: style can be found in header.less -->
	<?php print $ui->creamyHeader($user); ?>
	<!-- Left side column. contains the logo and sidebar -->
	<?php print $ui->getSidebar($user->getUserId(), $user->getUserName(), $user->getUserRole(), $user->getUserAvatar(), $_SESSION['usergroup']); ?>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header content-heading">
			<h1>
				<?php $lh->translateText("crm_title"); ?>
				<small><?php $lh->translateText("lead_log"); ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="./index.php"><i class="fa fa-phone"></i> <?php $lh->translateText("home"); ?></a></li>
				<li class="active"><?php $lh->translateText("crm_title"); ?>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<!-- <form id="search_form"> -->
					<form id="search_form">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="contacts_div">
									<table class="display" id="leadexlog"  style="width:100%">
										<thead>
											<tr>
												<th>ID</th>
												<th>default ID</th>
												<th>Full Name</th>
												<th>Middle Name</th>
												<th>Last Name</th>
												<th>Phone Number</th>
												<th>Person Export</th>
												<th>date Export</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>ID</th>
												<th>default ID</th>
												<th>Full Name</th>
												<th>Middle Name</th>
												<th>Last Name</th>
												<th>Phone Number</th>
												<th>Person Export</th>
												<th>date Export</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div><!-- /.body -->
						</div><!-- /.panel -->
					</form>
				</div><!-- /.col-lg-12 -->
			</div><!-- /. row -->
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
	<?php print $ui->getRightSidebar($user->getUserId(), $user->getUserName(), $user->getUserAvatar()); ?>
</div><!-- ./wrapper -->

<?php print $ui->standardizedThemeJS(); ?>

<!-- CHOSEN-->
<script src="js/dashboard/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<!-- BEGIN willson crm js -->
<script type="text/javascript" src="DataTables/Select-1.3.1/js/select.dataTables.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<!-- END willson crm js -->

<script type="text/javascript">
	$(document).ready(function() {
		// Setup - add a text input to each thead cell
		$('#leadexlog thead th').each( function () {
        	var title = $(this).text();
        	$(this).html( title + '<input type="text"  style="width:100%" />' );
		} );
		var table = $('#leadexlog').DataTable( {
			"dom"			: '<"top"Bl>rt<"bottom"ip><"clear">',
			"serverSide"	: true,
        	"select"		: true,
			"pagingType"	: "full_numbers",
			"aLengthMenu"	: [[10, 50, 100, 500, 1000, 2000, 5000], [10, 50, 100, 500, 1000, 2000, 5000]],
			"ajax"			: "./php/crm/lead_export_log_sp.php" ,
			// "buttons"		: 	[
			// 	{
			// 		text: 'View',
			// 		action: function () {
			// 			var url = './editcontacts.php';
			// 			var id = table.rows( { selected: true } ).data()[0][1];
			// 			var form = $('<form action="' + url + '" method="post"><input type="hidden" name="modifyid" value="'+id+'" /></form>');
			// 			$('body').append(form);  // This line is not necessary
			// 			$(form).submit();
			// 		}
			// 	}
			// ],
			// search by indi col
			"initComplete"	: function () {
				// Apply the search
				this.api().columns().every( function () {
					var that = this;
	
					$( 'input', this.header() ).on( 'keyup change clear', function () {
						if ( that.search() !== this.value ) {
							that
								.search( this.value )
								.draw();
						}
					} );
				} );
			},
			"columnDefs": [
				{
					// The `data` parameter refers to the data for the cell (defined by the
					// `data` option, which defaults to the column being worked with, in
					// this case `data: 2`.
					"render": function ( data, type, row ) {
						row[3] == null ? mid = '' : mid = row[3];
						row[4] == null ? last = '' : last = row[4];

						return data + ' '+ mid + ' ' + ' '+ last;
						// return data + ' '+ row[3] + ' ' + ' '+ row[4];
					},
					"targets": 2
				},
				{ "visible": false,  "targets": [ 3 ] },
				{ "visible": false,  "targets": [ 4 ] }
			]
		});
	});
</script>


</body>

</html>