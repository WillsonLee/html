<?php
/**
 * @file        goAddList.php
 * @brief       API to add new list
 * @copyright   Copyright (C) GOautodial Inc.
 * @author      Jeremiah Sebastian Samatra  <jeremiah@goautodial.com>
 * @author      Alexander Jim Abenoja  <alex@goautodial.com>
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
    
    include_once ("goAPI.php");
	
	// POST or GET Variables
	$chgid 											    = $astDB->escape($_REQUEST['chgid']);
	$version 											= $astDB->escape($_REQUEST['version']);
	$feature 										    = $astDB->escape($_REQUEST['feature']);
	$editor 											= $astDB->escape($_REQUEST['editor']);
	$date 									            = $astDB->escape($_REQUEST['date']);

    // Default values 
    $defActive 											= array("Y","N");
    
	// Error Checking
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		
		if ($goapiaccess > 0 && $userlevel > 7) {  			
			if (!checkIfTenant($log_group, $goDB)) {
				
                $SQLdate 							= date("Y-m-d H:i:s");
                $insertData 						= array(
                    'version' 						=> $version,
                    'feature' 						=> $feature,
                    'editor' 						=> $editor,
                    'date' 					        => $date
                );
                if($chgid != ''){
                    $astDB->where('chgid', $chgid);
                    $addResult 							= $astDB->update('go_change_log', $insertData);	
                    $log_id 							= log_action($goDB, 'ADD', $log_user, $ip_address, "updated Chg Log", $log_group, $astDB->getLastQuery());                    
                }else{
                    $addResult 							= $astDB->insert('go_change_log', $insertData);	
                    $log_id 							= log_action($goDB, 'ADD', $log_user, $ip_address, "Added Chg Log", $log_group, $astDB->getLastQuery());
                }                				
                
                if ($addResult == false) {
                    $err_msg 						= error_handle("10010");
                    $apiresults 					= array(
                        "code" 							=> "10010", 
                        "result" 						=> $err_msg
                    );
                    //$apiresults = array("result" => "Error: Failed to add");
                } else {
                    $apiresults 					= array(
                        "result" 						=> "success"
                    );
                }
			} else {
                $err_msg 									= error_handle("10001");
                $apiresults 								= array(
                    "code" 										=> "10001", 
                    "result" 									=> $err_msg
                );		
            }
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}

?>
