<?php
 /**
 * @file 		goUpdateDispo.php
 * @brief 		API for Agent UI
 * @copyright 	Copyright (C) GOautodial Inc.
 * @author     	Chris Lomuntad <chris@goautodial.com>
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$is_logged_in = check_agent_login($astDB, $goUser);

$agent = get_settings('user', $astDB, $goUser);
$system_settings = get_settings('system', $astDB);
$phone_settings = get_settings('phone', $astDB, $agent->phone_login, $agent->phone_pass);
$user = $agent->user;

if (isset($_GET['goSessionName'])) { $session_name = $astDB->escape($_GET['goSessionName']); }
    else if (isset($_POST['goSessionName'])) { $session_name = $astDB->escape($_POST['goSessionName']); }
if (isset($_GET['goServerIP'])) { $server_ip = $astDB->escape($_GET['goServerIP']); }
    else if (isset($_POST['goServerIP'])) { $server_ip = $astDB->escape($_POST['goServerIP']); }
if (isset($_GET['goDispoChoice'])) { $dispo_choice = $astDB->escape($_GET['goDispoChoice']); }
    else if (isset($_POST['goDispoChoice'])) { $dispo_choice = $astDB->escape($_POST['goDispoChoice']); }
if (isset($_GET['goLeadID'])) { $lead_id = $astDB->escape($_GET['goLeadID']); }
    else if (isset($_POST['goLeadID'])) { $lead_id = $astDB->escape($_POST['goLeadID']); }
if (isset($_GET['goAutoDialLevel'])) { $auto_dial_level = $astDB->escape($_GET['goAutoDialLevel']); }
    else if (isset($_POST['goAutoDialLevel'])) { $auto_dial_level = $astDB->escape($_POST['goAutoDialLevel']); }
if (isset($_GET['goAgentLogID'])) { $agent_log_id = $astDB->escape($_GET['goAgentLogID']); }
    else if (isset($_POST['goAgentLogID'])) { $agent_log_id = $astDB->escape($_POST['goAgentLogID']); }
if (isset($_GET['goCallBackDateTime'])) { $CallBackDatETimE = $astDB->escape($_GET['goCallBackDateTime']); }
    else if (isset($_POST['goCallBackDateTime'])) { $CallBackDatETimE = $astDB->escape($_POST['goCallBackDateTime']); }
if (isset($_GET['goListID'])) { $list_id = $astDB->escape($_GET['goListID']); }
    else if (isset($_POST['goListID'])) { $list_id = $astDB->escape($_POST['goListID']); }
if (isset($_GET['goRecipient'])) { $recipient = $astDB->escape($_GET['goRecipient']); }
    else if (isset($_POST['goRecipient'])) { $recipient = $astDB->escape($_POST['goRecipient']); }
if (isset($_GET['goUseInternalDNC'])) { $use_internal_dnc = $astDB->escape($_GET['goUseInternalDNC']); }
    else if (isset($_POST['goUseInternalDNC'])) { $use_internal_dnc = $astDB->escape($_POST['goUseInternalDNC']); }
if (isset($_GET['goUseCampaignDNC'])) { $use_campaign_dnc = $astDB->escape($_GET['goUseCampaignDNC']); }
    else if (isset($_POST['goUseCampaignDNC'])) { $use_campaign_dnc = $astDB->escape($_POST['goUseCampaignDNC']); }
if (isset($_GET['goMDnextCID'])) { $MDnextCID = $astDB->escape($_GET['goMDnextCID']); }
    else if (isset($_POST['goMDnextCID'])) { $MDnextCID = $astDB->escape($_POST['goMDnextCID']); }
if (isset($_GET['goStage'])) { $stage = $astDB->escape($_GET['goStage']); }
    else if (isset($_POST['goStage'])) { $stage = $astDB->escape($_POST['goStage']); }
if (isset($_GET['goCallbackID'])) { $vtiger_callback_id = $astDB->escape($_GET['goCallbackID']); }
    else if (isset($_POST['goCallbackID'])) { $vtiger_callback_id = $astDB->escape($_POST['goCallbackID']); }
if (isset($_GET['goPhoneNumber'])) { $phone_number = $astDB->escape($_GET['goPhoneNumber']); }
    else if (isset($_POST['goPhoneNumber'])) { $phone_number = $astDB->escape($_POST['goPhoneNumber']); }
if (isset($_GET['goPhoneCode'])) { $phone_code = $astDB->escape($_GET['goPhoneCode']); }
    else if (isset($_POST['goPhoneCode'])) { $phone_code = $astDB->escape($_POST['goPhoneCode']); }
if (isset($_GET['goDialMethod'])) { $dial_method = $astDB->escape($_GET['goDialMethod']); }
    else if (isset($_POST['goDialMethod'])) { $dial_method = $astDB->escape($_POST['goDialMethod']); }
if (isset($_GET['goUniqueID'])) { $uniqueid = $astDB->escape($_GET['goUniqueID']); }
    else if (isset($_POST['goUniqueID'])) { $uniqueid = $astDB->escape($_POST['goUniqueID']); }
if (isset($_GET['goCallBackLeadStatus'])) { $CallBackLeadStatus = $astDB->escape($_GET['goCallBackLeadStatus']); }
    else if (isset($_POST['goCallBackLeadStatus'])) { $CallBackLeadStatus = $astDB->escape($_POST['goCallBackLeadStatus']); }
if (isset($_GET['goComments'])) { $comments = $astDB->escape($_GET['goComments']); }
    else if (isset($_POST['goComments'])) { $comments = $astDB->escape($_POST['goComments']); }
if (isset($_GET['goCustomFieldNames'])) { $FORMcustom_field_names = $astDB->escape($_GET['goCustomFieldNames']); }
    else if (isset($_POST['goCustomFieldNames'])) { $FORMcustom_field_names = $astDB->escape($_POST['goCustomFieldNames']); }
if (isset($_GET['goCallNotes'])) { $call_notes = $astDB->escape($_GET['goCallNotes']); }
    else if (isset($_POST['goCallNotes'])) { $call_notes = $astDB->escape($_POST['goCallNotes']); }
if (isset($_GET['goQMDispoCode'])) { $qm_dispo_code = $astDB->escape($_GET['goQMDispoCode']); }
    else if (isset($_POST['goQMDispoCode'])) { $qm_dispo_code = $astDB->escape($_POST['goQMDispoCode']); }
if (isset($_GET['goEmailEnabled'])) { $email_enabled = $astDB->escape($_GET['goEmailEnabled']); }
    else if (isset($_POST['goEmailEnabled'])) { $email_enabled = $astDB->escape($_POST['goEmailEnabled']); }

$MT[0] = '';
$MAN_vl_insert = 0;
$StarTtime = date("U");
$errorcnt = 0;

$CBcomments = '';
$CBuser = '';
$CBcallback_time = '';
$CBentry_time = '';

if ($is_logged_in) {

	if($dispo_choice == 'DispoSelect'){
		$updateData = array(
			'lead_id' => $lead_id,
			'status' => 'PAUSED'
		);
		$astDB->where('user', $user);
		$astDB->where('server_ip', $server_ip);
		$rslt = $astDB->update('vicidial_live_agents', $updateData);
		// error_log('sql update vicidial_live_agents pause----->>> ' . $astDB->getLastQuery() );

		 //$stmt="SELECT entry_time,callback_time,user,comments FROM vicidial_callbacks where lead_id='$lead_id' order by callback_id desc LIMIT 1;";
		 $astDB->where('lead_id', $lead_id);
		 $astDB->orderBy('callback_id', 'desc');
		 $rslt = $astDB->getOne('vicidial_callbacks', 'entry_time,callback_time,user,comments,callback_id');
		 $cb_record_ct = $astDB->getRowCount();
		 if ($cb_record_ct > 0) {
			 $row = $rslt;
			 $CBentry_time =		trim("{$row['entry_time']}");
			 $CBcallback_time =	trim("{$row['callback_time']}");
			 $CBuser =			trim("{$row['user']}");
			 $CBcomments =		trim("{$row['comments']}");
			 $CBack_id =		    trim("{$row['callback_id']}");
			 
			 $astDB->where('callback_id', $CBack_id);
			 $astDB->where('user', $user);
			 $astDB->where('status', 'INACTIVE', '!=');
			 $query = $astDB->update('vicidial_callbacks', array('status'=>'INACTIVE'));
		 }
	}
	else if ( (strlen($dispo_choice)<1) || (strlen($lead_id)<1) ) {
		$message = "Dispo Choice or Lead ID is NOT valid";
		$errorcnt++;
	} else {
	
		// error_log('else -----> ' .$dispo_choice);
		
		if ($auto_dial_level < 1) {
			//$stmt = "UPDATE vicidial_live_agents set status='PAUSED',callerid='' where user='$user';";
			$updateData = array(
				'status' => 'PAUSED',
				'callerid' => ''
			);
			$astDB->where('user', $user);
			$rslt = $astDB->update('vicidial_live_agents', $updateData);
			// error_log('sql update vicidial_live_agents----->>> ' . $astDB->getLastQuery() );
		}
		//$stmt="UPDATE vicidial_list set status='$dispo_choice', user='$user' where lead_id='$lead_id';";
		$updateData = array(
			'status' => $dispo_choice,
			'last_local_call_time' => $NOW_TIME,
			'user' => $user
		);
		$astDB->where('lead_id', $lead_id);
		$rslt = $astDB->update('vicidial_list', $updateData);
		
		//get phone number by lead id
		$astDB->where('lead_id', $lead_id);
		$rslt = $astDB->getOne('vicidial_list', 'phone_number,phone_code,list_id');
		$astDB->where('phone_number', $rslt['phone_number']);
		$astDB->update('group_list_'.$agent->user_group, $updateData);
		// error_log('sql update group_list_----->>> ' . $astDB->getLastQuery() );		
		// error_log('sql update depo----->>> ' . $astDB->getLastQuery() );

		//add dispo to vicidial_log
		if(substr($dispo_choice, 0, 6) == 'CBHOLD'){
			
			if($dispo_choice == 'CALLBK'){
				$dispo_choice_log = $dispo_choice;
			}else{
				$dispo_choice_log = substr("$dispo_choice", 7);
			}
		}else{
			$dispo_choice_log = $dispo_choice;
		}
		$PADlead_id = sprintf("%010s", $lead_id);
		while (strlen($PADlead_id) > 9) {$PADlead_id = substr("$PADlead_id", 1);}
		$FAKEcall_id = "$StarTtime.$PADlead_id";
		$insertData = array(
			'uniqueid' => $FAKEcall_id,
			'lead_id' => $lead_id,
			'list_id' => $rslt['list_id'],
			'campaign_id' => $campaign,
			'call_date' => $NOW_TIME,
			'start_epoch' => $StarTtime,
			'end_epoch' => $StarTtime,
			'length_in_sec' => '0',
			'status' => $dispo_choice_log,
			'prestatus' => 'w',
			'phone_code' =>  $rslt['phone_code'],
			'phone_number' =>  $rslt['phone_number'],
			'user' => $user,
			'comments' => 'MANUAL',
			'processed' => 'N',
			'user_group' => $agent->user_group,
			'term_reason' => 'AGENT',
			'alt_dial' => 'MAIN'
		);
		$rslt = $astDB->insert('vicidial_log', $insertData);


		//Added by Poundteam Incorporated for Audit Comments Package');
		audit_comments($astDB, $lead_id, $list_id, $format, $user, $NOW_TIME, $server_ip, $session_name, $campaign);
	
		$log_dispo_choice = $dispo_choice;
		if (strlen($CallBackLeadStatus) > 0) {$log_dispo_choice = $CallBackLeadStatus;}
		
	}
	
    
	if ($errorcnt < 1) {
		$dispo_sec = 0;
		$StarTtime = date("U");
		$user_group = '';
		//$stmt="SELECT user_group FROM vicidial_users where user='$user' LIMIT 1;";
		$astDB->where('user', $user);
		$rslt = $astDB->getOne('vicidial_users', 'user_group');
		$ug_record_ct = $astDB->getRowCount();
		if ($ug_record_ct > 0) {
			$user_group = trim("{$rslt['user_group']}");
		}
		$CALL_agent_log_id = $agent_log_id;
		
		if ($auto_dial_level < 1) {
			$insertData = array(
				'user' => $user,
				'server_ip' => $server_ip,
				'event_time' => $NOW_TIME,
				'campaign_id' => $campaign,
				'pause_epoch' => $StarTtime,
				'pause_sec' => '0',
				'wait_epoch' => $StarTtime,
				'user_group' => $user_group
			);
			if ($MAN_vl_insert > 0) {
				$MAN_insert_leadIDsql = array(
					'lead_id' => $lead_id
				);
				$insertData = array_merge($insertData, $MAN_insert_leadIDsql);
			}
			//$stmt="INSERT INTO vicidial_agent_log SET user='$user',server_ip='$server_ip',event_time='$NOW_TIME',campaign_id='$campaign',pause_epoch='$StarTtime',pause_sec='0',wait_epoch='$StarTtime',user_group='$user_group'$MAN_insert_leadIDsql;";
			$rslt = $astDB->insert('vicidial_agent_log', $insertData);
			$affected_rows = $astDB->getRowCount();
			$agent_log_id = $astDB->getInsertId();
			// error_log('sql update vicidial_agent_log----->>> ' . $astDB->getLastQuery() );
		
			//$stmt="UPDATE vicidial_live_agents SET agent_log_id='$agent_log_id' where user='$user';";
			$updateData = array(
				'agent_log_id' => $agent_log_id
			);
			$astDB->where('user', $user);
			$rslt = $astDB->update('vicidial_live_agents', $updateData);
			// error_log('sql update vicidial_live_agents agent_log_id----->>> ' . $astDB->getLastQuery() );
		}
		//stay
		### CALLBACK ENTRY
		$astDB->where('lead_id', $lead_id);
		$astDB->where('campaign_id', $campaign);
		$astDB->where('user', $user);
		$astDB->orderBy('callback_id', 'desc');
		$check_query = $astDB->getOne('vicidial_callbacks', 'callback_id');
		
		// error_log('sql get callback id vicidial_callbacks----->>> ' . $astDB->getLastQuery() );
		$accallback_id = $check_query['callback_id'];

		if ( (substr($dispo_choice, 0, 6) == 'CBHOLD') and (strlen($CallBackDatETimE) > 10) ) {
			$comments = urldecode($comments);
			$comments = preg_replace('/"/i', '', $comments);
			$comments = preg_replace("/'/i", '', $comments);
			$comments = preg_replace('/;/i', '', $comments);
			$comments = preg_replace("/\\\\/i", ' ', $comments);
			//$stmt="INSERT INTO vicidial_callbacks (lead_id,list_id,campaign_id,status,entry_time,callback_time,user,recipient,comments,user_group,lead_status) values('$lead_id','$list_id','$campaign','ACTIVE','$NOW_TIME','$CallBackDatETimE','$user','$recipient','$comments','$user_group','$CallBackLeadStatus');";
			$insertData = array(
				'lead_id' => $lead_id,
				'campaign_id' => $campaign,
				'status' => 'ACTIVE',
				'entry_time' => $NOW_TIME,
				'callback_time' => $CallBackDatETimE,
				'user' => $user,
				'recipient' => $recipient,
				'comments' => $comments,
				'user_group' => $user_group,
				'lead_status' => $CallBackLeadStatus
			);
			if(strlen($accallback_id)<1){
				error_log('$accallback_id----->>> ' . $accallback_id );
				error_log('strlen($accallback_id)----->>> ' . strlen($accallback_id) );
				$rslt = $astDB->insert('vicidial_callbacks', $insertData);
			}else{
				$astDB->where('callback_id', $accallback_id);
				$rslt = $astDB->update('vicidial_callbacks', $insertData);
			}
			error_log('sql update vicidial_callbacks----->>> ' . $astDB->getLastQuery() );
			
		}else{
			$insertData = array(
			'status' => 'INACTIVE',
			);
			if(!strlen($accallback_id)<1){
				$astDB->where('callback_id', $accallback_id);
				$rslt = $astDB->update('vicidial_callbacks', $insertData);
			}
			// error_log('sql update INACTIVE vicidial_callbacks----->>> ' . $astDB->getLastQuery() );

		}
		$data_InfO = array(
			"lead_id" => $lead_id, 
			"dispo_choice" => $dispo_choice, 
			"agent_log_id" => $agent_log_id,
			'CBcomments' => $CBcomments,
			'CBuser' => $CBuser,
			'CBcallback_time' => $CBcallback_time,
			'CBentry_time' => $CBentry_time
		);
		$APIResult = array( "result" => "success", "message" => "Lead {$lead_id} has been changed to {$dispo_choice} status", "data" => $data_InfO );
	} else {
		$APIResult = array( "result" => "error", "message" => $message );
	}
} else {
    $APIResult = array( "result" => "error", "message" => "Agent '$goUser' is currently NOT logged in" );
}
?>
