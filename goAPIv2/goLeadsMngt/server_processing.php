<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'vicidial_lists';

// Table's primary key
$primaryKey = 'lead_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns2 = array(
	array( 'db' => 'first_name', 'dt' => 0 ),
	array( 'db' => 'last_name',  'dt' => 1 ),
	array( 'db' => 'position',   'dt' => 2 ),
	array( 'db' => 'office',     'dt' => 3 ),
	array(
		'db'        => 'start_date',
		'dt'        => 4,
		'formatter' => function( $d, $row ) {
			return date( 'jS M y', strtotime($d));
		}
	),
	array(
		'db'        => 'salary',
		'dt'        => 5,
		'formatter' => function( $d, $row ) {
			return '$'.number_format($d);
		}
	)
);
$columns = array(
	array( 'db' => 'lead_id', 'dt' => 0 ),
	array( 'db' => 'first_name',  'dt' => 1 ),
	array( 'db' => 'phone_number',   'dt' => 2 ),
	array( 'db' => 'nric',     'dt' => 3 ),
	array( 'db' => 'race',     'dt' => 4 ),
	array( 'db' => 'address1',     'dt' => 5 ),
	array( 'db' => 'postal_code',     'dt' => 6 )
);
// SQL server connection information
$sql_details = array(
	'user' => 'root',
	'pass' => 'vicidialnow',
	'db'   => 'asterisk',
	'host' => '121.121.46.244'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );
error_log('server---------');
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	//SSP::simple2( $columns )
);


