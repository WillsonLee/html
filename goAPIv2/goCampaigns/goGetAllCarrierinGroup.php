<?php
/**
 * @file 		goGetAllCarrier.php
 * @brief 		API to get all Carrier Lists 
 * @copyright 	Copyright (c) 2018 GOautodial Inc.
 * @author      Demian Lizandro A. Biscocho 
 * @author     	Alexander Jim H. Abenoja
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
    include_once ("goAPI.php");
    // include_once ("../licensed-conf.php");

    // POST or GET Variables
    $user_group 										= $astDB->escape($_REQUEST["user_group"]);	
	
	// Error Checking
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		
		if ($goapiaccess > 0 && $userlevel > 7) {	
			// set tenant value to 1 if tenant - saves on calling the checkIfTenantf function
			// every time we need to filter out requests
			$tenant										= (checkIfTenant($log_group, $goDB)) ? 1 : 0;
			
			if ($tenant) {
				$astDB->where("uc.user_group", $user_group);
				//$astDB->orWhere("user_group", "---ALL---");
			} else {
				if (strtoupper($log_group) != 'ADMIN') {
					$astDB->where("uc.user_group", $log_group);
				}else{
					$astDB->where("uc.user_group", $user_group);
				}					
			}
			
			// get carrier list
			$cols 										= array(
				"uc.user_group", 
				"uc.carrier_id", 
				"uc.carrier_name",
				"server_ip", 
				"protocol", 
				"registration_string", 
				"active", 
				"dialplan_entry"
			);
			//$astDB->where("user_group", $user_group);
			$astDB->join('vicidial_server_carriers oc','oc.carrier_id = uc.carrier_id','LEFT');
			$astDB->orderBy('carrier_id', 'ASC');
			$query = $astDB->get("vicidial_usergroup_allowed_carrier uc", NULL, $cols);

			//this is for edittelephonycampaign.php use 
			// if lvl8 admin use and 1st carrier id = 0 then run below
			if($query[0]['carrier_id'] == '0' && $log_group == $user_group){
				$astDB->orderBy('carrier_id', 'ASC');
				$query = $astDB->get("vicidial_server_carriers uc", NULL, $cols);
			}

			if ($astDB->count > 0) {			
				foreach ($query as $fresults) {
					$dataUserGroup[] 					= $fresults['user_group'];	
					$dataCarrierID[] 					= $fresults['carrier_id'];
					$dataCarrierName[] 					= $fresults['carrier_name'];
					$dataServerIp[] 					= $fresults['server_ip'];
					$dataProtocol[] 					= $fresults['protocol'];
					$dataRegistrationString[] 			= $fresults['registration_string'];
					$dataActive[] 						= $fresults['active'];
					$dataDialPlanEntry[] 				= $fresults['dialplan_entry'];  
				}
					
				$apiresults 							= array(
					"result" 								=> "success", 
					"user_group" 							=> $dataUserGroup,
					"carrier_id" 							=> $dataCarrierID,
					"carrier_name" 							=> $dataCarrierName, 
					"server_ip" 							=> $dataServerIp, 
					"protocol" 								=> $dataProtocol, 
					"registration_string" 					=> $dataRegistrationString, 
					"active" 								=> $dataActive, 
					"dialplan_entry" 						=> $dataDialPlanEntry
				);					
			} else {
				$err_msg 								= error_handle("10010");
				$apiresults 							= array(
					"code" 									=> "10010", 
					"result" 								=> $err_msg
				); 
			}		
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}

?>
