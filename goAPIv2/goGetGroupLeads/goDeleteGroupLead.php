<?php
 /**
 * @file 		goDeleteGroupLead.php
 * @brief 		API for Deleting Group Leads
 * @copyright 	Copyright (C) GOautodial Inc.
 * @author     	Alexander Abenoja  <alex@goautodial.com>
 * @author     	Chris Lomuntad  <chris@goautodial.com>
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


    include_once ("goAPI.php");
 
    // POST or GET Variables
    $string_def_id 											= $astDB->escape($_REQUEST['def_id']);
	$def_id = explode (",", $string_def_id); 

	// ERROR CHECKING 
	if (empty($goUser) || is_null($goUser)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI User Not Defined."
		);
	} elseif (empty($goPass) || is_null($goPass)) {
		$apiresults 									= array(
			"result" 										=> "Error: goAPI Password Not Defined."
		);
	} elseif (empty($log_user) || is_null($log_user)) {
		$apiresults 									= array(
			"result" 										=> "Error: Session User Not Defined."
		);
	} elseif (empty($def_id) || is_null($def_id)) {
		$err_msg 										= error_handle("40001");
		$apiresults 									= array(
			"code" 											=> "40001", 
			"result" 										=> $err_msg
		);
	} else {
		// check if goUser and goPass are valid
		$fresults										= $astDB
			->where("user", $goUser)
			->where("pass_hash", $goPass)
			->getOne("vicidial_users", "user,user_level");
		
		$goapiaccess									= $astDB->getRowCount();
		$userlevel										= $fresults["user_level"];
		
		if ($goapiaccess > 0 && $userlevel > 7) {	
			// check def_id if it exists
			$astDB->where('def_id', $def_id, 'IN');
			$fresults 									= $astDB->getOne("group_list_".$log_group, "def_id");
			
			if ($fresults) {

				$astDB->where('def_id', $def_id, 'IN');
				$astDB->delete("group_list_".$log_group);

				$log_id 								= log_action($goDB, 'DELETE', $log_user, $log_ip, "Deleted Default ID: $def_id", $log_group, $astDB->getLastQuery());
				
				$apiresults								= array(
					"result" 								=> "success"
				);
			} else {
				$err_msg 								= error_handle("10010");
				$apiresults 							= array(
					"code" 									=> "10010", 
					"result" 								=> $err_msg
				);
				//$apiresults = array("result" => "Error: Default ID does not exist.");
			}		
		} else {
			$err_msg 									= error_handle("10001");
			$apiresults 								= array(
				"code" 										=> "10001", 
				"result" 									=> $err_msg
			);		
		}
	}
	
?>
