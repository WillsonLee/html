<?php
 /**
	 * @file 		goUploadMe.php
	 * @brief 		API for Uploading Leads
	 * @copyright 	Copyright (C) GOautodial Inc.
	 * @author		Jericho James Milo  <james@goautodial.com>
	 * @author     	Chris Lomuntad  <chris@goautodial.com>
	 *
	 * @par <b>License</b>:
	 *  This program is free software: you can redistribute it and/or modify
	 *  it under the terms of the GNU Affero General Public License as published by
	 *  the Free Software Foundation, either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  This program is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU Affero General Public License for more details.
	 *
	 *  You should have received a copy of the GNU Affero General Public License
	 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	//define('__ROOT__', dirname(dirname(__FILE__)));
	include_once ("goAPI.php");

	ini_set('memory_limit','2048M');
	ini_set('upload_max_filesize', '2048M');
	ini_set('post_max_size', '2048M');
    ini_set('max_execution_time', 3600);
	
	//ini_set('display_errors', 1);
	//error_reporting(E_ALL);
	
	$thefile = $_FILES['goFileMe']['tmp_name'];
	$theList = $astDB->escape($_REQUEST["goListId"]);
	$goDupcheck = $astDB->escape($_REQUEST["goDupcheck"]);
	$goCountInsertedLeads = 0;
	$default_delimiter = ",";
	$phone_code_override = $astDB->escape($_REQUEST["phone_code_override"]);
	
	$lead_mapping = NULL;
	if(!empty($_REQUEST["lead_mapping"]))	
		$lead_mapping = $astDB->escape($_REQUEST["lead_mapping"]);

	$alex = array();
	$goGetCheckcustomFieldNamesCorrect = ""; //constant
	
	// path where your CSV file is located
	define('CSV_PATH','/tmp/');

	error_log(" upload start, count <<<-- ") ;
	//insert upload status to group_list_status
	$start_date = date("Y-m-d H:i:sa");
	$sdate = date("Y-m-d H:i:s");
	
	$updata = 'usr group = ' . $log_group .' | user = '.$log_user .'';
	writedata($updata);
		
	// $insertData = array(
	// 	'user' => $log_user,
	// 	'group_name' => $log_group,
	// 	'status' => "processing"
	// );
	// $astDB->insert("group_list_status", $insertData);
	// $update_use_last_id = $astDB->getInsertId();

	// Name of your CSV file
	//$csv_file = CSV_PATH . "$thefile"; 
	$csv_file = $thefile;
	$counter = 0;
	// REPLACE DELIMITER to SEMI-COLON -- CUSTOMIZATION!!!!!
        if(!empty($_REQUEST["custom_delimiter"]) && isset($_REQUEST["custom_delimiter"])){
           //$default_delimiter = $_REQUEST["custom_delimiter"];
	   $delimiters = explode(" ", $_REQUEST["custom_delimiter"]);
           $str = file_get_contents($csv_file);
           $str1 = str_replace($delimiters, $default_delimiter, $str);
           file_put_contents($csv_file, $str1);
        }

	// REGEX to prevent weird characters from ending up in the fields
        $field_regx = "/['\"`\\;]/";
        $field_regx = str_replace($delimiters, "", $field_regx);

	$duplicates = 0;
	$merge = 0;
	$over = 0;
	$every1k = 1000;

	$astDB->where('user', $log_user);
	$touserid = $astDB->getOne("vicidial_users", 'user_id');
	$tousrid = $touserid["user_id"];
	//get system user id, cos not all user system are same id
	$astDB->where('user', "system");
	$fromuserid = $astDB->getOne("vicidial_users", 'user_id');
	$fromusrid = $fromuserid["user_id"];
	$date = date("Y-m-d H:i:s");

	//die($theList."<br>".$thefile."<br>".$csv_file);
	if (($handle = fopen($csv_file, "r")) !== FALSE) {
		$getHeder = fgetcsv($handle);
		//$goInsertSuccess = 0;
			//$array 21 last column
			
			//for custom fields start GLOBAL varaibles
			// $goCountTheHeader = count($getHeder);
			
			// if($goCountTheHeader > 21 && !empty($lead_mapping)) {
			// 	for($x=21; $x < count($getHeder); $x++) {
			// 		$goGetLastHeader .= $x.","; #get digits for specific data
			// 		$goGetLastCustomFiledsName .= $getHeder[$x].","; #get the digits for specific custom field
			// 	}
				
			// 	$goGetLastHeader = preg_replace("/,$/",'',$goGetLastHeader);
			// 	$goGetLastHeader2 = explode(",",$goGetLastHeader);
			// 	$goGetLastCustomFiledsName = preg_replace("/,$/",'',$goGetLastCustomFiledsName);
			// 	$goGetLastCustomFiledsName2 = explode(",",$goGetLastCustomFiledsName);
					
			// } elseif($goCountTheHeader > 21) {
			// 	for($x=21; $x < count($getHeder); $x++) {
			// 		$goGetLastHeader .= $x.","; #get digits for specific data
			// 		$goGetLastCustomFiledsName .= $getHeder[$x].","; #get the digits for specific custom field
			// 	}
				
			// 	$goGetLastHeader = preg_replace("/,$/",'',$goGetLastHeader);
			// 	$goGetLastHeader2 = explode(",",$goGetLastHeader);
				
			// 	$goGetLastCustomFiledsName = preg_replace("/,$/",'',$goGetLastCustomFiledsName);
			// 	$goGetLastCustomFiledsName2 = explode(",",$goGetLastCustomFiledsName);
				
			// 	# check custom field names are correct
			// 	$goGetLastCustomFiledsNameWithLeadID = "lead_id,".$goGetLastCustomFiledsName;
			// 	$goGetCheckcustomFieldNamesCorrect = goCheckCustomFieldsName($astDB, $theList, $goGetLastCustomFiledsNameWithLeadID);
				
			// 	if($goGetCheckcustomFieldNamesCorrect === "error") {
			// 		fclose($handle);
			// 	}
			// }
		//end for custom fields start GLOBAL varaibles
		
		error_log('start own group upload list');
		//error_log('fgetcsv($handle, 1000, $default_delimiter) -->> ' . fgetcsv($handle, 1000, $default_delimiter));
		while (($data = fgetcsv($handle, 1000, $default_delimiter)) !== FALSE) {
			$num = count($data);
			for ($c=0; $c < $num; $c++) {
				$col[$c] = $data[$c];
			}
			# REGEX to prevent weird characters from ending up in the fields
			$field_regx = "/['\"`\\;]/";
			
			# SQL Query to insert data into DataBase
			$entry_date = date("Y-m-d H:i:s");
			
			$status = "NEW";
			$vendor_lead_code = preg_replace($field_regx, "", $col[1]);
			$list_id = $theList;
			$gmt_offset = "0";
			// PHONE CODE OVERRIDE
			if(!empty($phone_code_override))
				$phone_code = preg_replace($field_regx, "", $phone_code_override);
			else
				$phone_code = preg_replace($field_regx, "", $col[2]);
				$phone_number = preg_replace($field_regx, "", $col[0]);
				$title = preg_replace($field_regx, "", $col[3]);
				$first_name = preg_replace($field_regx, "", $col[4]);
				$middle_initial = preg_replace($field_regx, "", $col[5]);
				$last_name = preg_replace($field_regx, "", $col[6]);
				$nric = preg_replace($field_regx, "", $col[7]);
				$race = preg_replace($field_regx, "", $col[8]);
				$income = preg_replace($field_regx, "", $col[9]);
				$address1 = preg_replace($field_regx, "", $col[10]);
				$address2 = preg_replace($field_regx, "", $col[11]);
				$address3 = preg_replace($field_regx, "", $col[12]);
				$city = preg_replace($field_regx, "", $col[13]);
				$state = preg_replace($field_regx, "", $col[14]);
				$province = preg_replace($field_regx, "", $col[15]);
				$postal_code = preg_replace($field_regx, "", $col[16]);
				$country_code = preg_replace($field_regx, "", $col[17]);
				$gender = preg_replace($field_regx, "", $col[18]);
				$date_of_birth = preg_replace($field_regx, "", $col[19]);
				$date_of_birth = date("Y-m-d", strtotime($date_of_birth));
				$alt_phone = preg_replace($field_regx, "", $col[20]);
				$email = preg_replace($field_regx, "", $col[21]);
				$security_phrase = preg_replace($field_regx, "", $col[22]);
				$comments = preg_replace($field_regx, "", $col[23]);
				$entry_list_id = 0;
				$called_since_last_reset = "N";

				// LEAD MAPPING -- CUSTOMIZATION!!!!!
			if(!empty($lead_mapping)){
				$lead_mapping_data = explode(",",$_REQUEST["lead_mapping_data"]);
				$lead_mapping_fields = explode(",", $_REQUEST["lead_mapping_fields"]);
				$standard_fields = array("Phone","VendorLeadCode","PhoneCode","Title","FirstName","MiddleInitial","LastName","nric","race","income","Address1","Address2","Address3","City","State","Province","PostalCode","CountryCode","Gender","DateOfBirth","AltPhone","Email","SecurityPhrase","Comments");
				// MAKE MAP FIELDS AN INDEX OF MAP DATA & SEPARATE STANDARD FROM CUSTOM ARRAYS

				for($l=0; $l < count($lead_mapping_fields);$l++){
					if(in_array($lead_mapping_fields[$l], $standard_fields))
						$standard_array[$lead_mapping_fields[$l]] = $lead_mapping_data[$l];
					else
						$custom_array[$lead_mapping_fields[$l]] = $lead_mapping_data[$l];
				}
				//set default values to none
				$phone_number = "";
                $vendor_lead_code = "";
				if(!empty($phone_code_override))
					$phone_code = $phone_code_override;
				else
                    $phone_code = 1;
					$log = $phone_code_override;
					$title = "";
					$first_name = "";
					$middle_initial = "";
					$last_name = "";
					$nric = "";
					$race = "";
					$income = "";
					$address1 = "";
					$address2 = "";
					$address3 = "";
					$city = "";
					$state = "";
					$province = "";
					$postal_code = "";
					$country_code = "";
					$gender = "";
					$date_of_birth = "";
					$alt_phone = "";
					$email = "";
					$security_phrase = "";
					$comments = "";				
				
				//get arrayed lead mapping requests
				foreach($standard_array as $l => $map_data){
					//$logthis[] = $map_data;
					if($map_data !== "" || $map_data !== "."){
						// one by one sort through columns to overwrite lead mapping data
						if($l == "Phone")
							$phone_number = $col[$map_data];
						if($l == "VendorLeadCode")
							$vendor_lead_code = $col[$map_data];
						if($l == "PhoneCode"){
							if(!empty($phone_code_override))
								$phone_code = $phone_code_override;
							else
								$phone_code = $col[$map_data];
						}if($l == "Title")
							$title = $col[$map_data];
						if($l == "FirstName")
							$first_name = $col[$map_data];
							$first_name = preg_replace("/[\r\n]*/","",$first_name);
						if($l == "MiddleInitial")
							$middle_initial = $col[$map_data];
							$middle_initial = preg_replace("/[\r\n]*/","",$middle_initial);
						if($l == "LastName")
							$last_name = $col[$map_data];
							$last_name = preg_replace("/[\r\n]*/","",$last_name);
						if($l == "nric")
							$nric = $col[$map_data];
						if($l == "race")
							$race = $col[$map_data];
						if($l == "income")
							$income = $col[$map_data];
						if($l == "Address1")
							$address1 = $col[$map_data];
							$address1 = preg_replace("/[\r\n]*/","",$address1);
						if($l == "Address2")
							$address2 = $col[$map_data];
							$address2 = preg_replace("/[\r\n]*/","",$address2);
						if($l == "Address3")
							$address3 = $col[$map_data];
							$address3 = preg_replace("/[\r\n]*/","",$address3);
						if($l == "City")
							$city = $col[$map_data];
						if($l == "State")
							$state = $col[$map_data];
						if($l == "Province")
							$province = $col[$map_data];
						if($l == "PostalCode")
							$postal_code = $col[$map_data];
						if($l == "CountryCode")
							$country_code = $col[$map_data];
						if($l == "Gender")
							$gender = $col[$map_data];
						if($l == "DateOfBirth")
							$date_of_birth = $col[$map_data];
						if($l == "AltPhone")
							$alt_phone = $col[$map_data];
						if($l == "Email")
							$email = $col[$map_data];
						if($l == "SecurityPhrase")
							$security_phrase = $col[$map_data];
						if($l == "Comments")
							$comments = $col[$map_data];
					}// end if
				}// end loop
			} // END OF LEAD MAPPING
			
			if($goDupcheck == "DUPLISTMERGE") {
				#Duplicate check within the LIST
				if($goGetCheckcustomFieldNamesCorrect === "error" && empty($lead_mapping)) {
					fclose($handle);
				} else {
					$tbl_name = 'group_list_' . $log_group;
					$astDB->where('phone_number', $phone_number);
					$CheckDup = $astDB->get($tbl_name, null, '*');
					$countResult = $astDB->getRowCount();

					////check in vicidial_dnc
					//$astDB->where('phone_number', $phone_number);
					//$resultCheckPhone = $astDB->getOne('vicidial_dnc', 'phone_number');
					//$countResult2 = $astDB->getRowCount();
						
					if($countResult < 1) {
						$USarea = substr($phone_number, 0, 3);
						$gmt_offset = lookup_gmt($astDB, $phone_code,$USarea,$state,$LOCAL_GMT_OFF_STD,$Shour,$Smin,$Ssec,$Smon,$Smday,$Syear,$postalgmt,$postal_code,$owner);
						
						//$goQueryInsDupList = "INSERT INTO vicidial_list (lead_id, entry_date, status, vendor_lead_code, list_id, gmt_offset_now, phone_code, phone_number, title, first_name, middle_initial, last_name, nric, race, income, address1, address2, address3, city, state, province, postal_code, country_code, gender, date_of_birth, alt_phone, email, security_phrase, comments, entry_list_id) VALUES ('', '$entry_date', '$status', '$vendor_lead_code', '$list_id', '$gmt_offset', '$phone_code', '$phone_number', '$title',	'$first_name', '$middle_initial', '$last_name', '$nric', '$race', '$income',	'$address1', '$address2', '$address3', '$city',	'$state', '$province', '$postal_code', '$country_code',	'$gender', '$date_of_birth', '$alt_phone', '$email', '$security_phrase', '$comments', '$entry_list_id');";
						$insertData = array(
							'lead_id' => '',
							'entry_date' => $entry_date,
							'status' => $status,
							'vendor_lead_code' => $vendor_lead_code,
							'list_id' => $list_id,
							'gmt_offset_now' => $gmt_offset,
							'phone_code' => $phone_code,
							'phone_number' => $phone_number,
							'title' => $title,
							'first_name' => $first_name,
							'middle_initial' => $middle_initial,
							'last_name' => $last_name,
							'nric' => $nric,
							'race' => $race,
							'income' => $income,
							'address1' => $address1,
							'address2' => $address2,
							'address3' => $address3,
							'city' => $city,
							'state' => $state,
							'province' => $province,
							'postal_code' => $postal_code,
							'country_code' => $country_code,
							'gender' => $gender,
							'date_of_birth' => $date_of_birth,
							'alt_phone' => $alt_phone,
							'email' => $email,
							'security_phrase' => $security_phrase,
							'comments' => $comments,
							'entry_list_id' => $entry_list_id,
							'last_local_call_time' => '0000-00-00 00:00:00'
						);
						// upload group list get gtoup name
						$tbl_name = 'group_list_' . $log_group;
						$rsltGoQueryInsDupList = $astDB->insert($tbl_name, $insertData);
						$goLastInsertedLeadIDDUPLIST = $astDB->getInsertId();
						$alex["insertquery"] = $astDB->getLastQuery();
						# start set query for custom fields
							// if(!empty($lead_mapping) && !empty($custom_array)){ // LEAD MAPPING CUSTOMIZATION
							// 	$goCustomKeyData = array();
							// 	$goCustomValuesData = array();
							// 	$goCustomUpdateData = array();

							// 	foreach($custom_array as $custom_key => $map_data){
							// 		$goCustomValues = $col[$map_data];
							// 		array_push($goCustomKeyData, "$custom_key");
							// 		array_push($goCustomValuesData, "'$goCustomValues'");
							// 		array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");

							// 		//$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_key) VALUES('$goLastInsertedLeadIDNODUP', '$goCustomValues') ON DUPLICATE KEY UPDATE $custom_key='$goCustomValues'";
							// 		//$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// 	}

							// 	$custom_keyValues = implode(",", $goCustomKeyData);
							// 	$goCustomValues = implode(",", $goCustomValuesData);
							// 	$goCustomUpdate = implode(", ",  $goCustomUpdateData);

							// 	$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_keyValues) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";
							// 	$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// }elseif($goCountTheHeader > 21) {
							// 	$goShowCustomFields = "DESC custom_$list_id;";
							// 	$rsltgoShowCustomFields = $astDB->rawQuery($goShowCustomFields);
							// 	$countResultrsltgoShowCustomFields = $astDB->getRowCount();
								
							// 	if($countResultrsltgoShowCustomFields > 1) {
							// 		$totalExplode = count($goGetLastHeader2);

							// 		$goCustomValuesData = array();
							//                                     $goCustomUpdateData = array();

							// 		for($ax=0; $ax < $totalExplode; $ax++) {
							// 			$goHeaderOfCustomFields = $goGetLastCustomFiledsName2[$ax]; #get the header name of the custom fields
							// 			$goCustomValues = $col[$goGetLastHeader2[$ax]]; #get the values of the custom fields
											
							// 			#$goQueryCustomFields = "INSERT INTO custom_$theList (lead_id,".$goHeaderOfCustomFields.") VALUES ('$goLastInsertedLeadIDDUPLIST','".$goCustomValues."');";
							// 			#$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', '$goCustomValues') ON DUPLICATE KEY UPDATE $goHeaderOfCustomFields='$goCustomValues'";
							// 			#$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
										
							// 			#$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads");
							// 			array_push($goCustomValuesData, "'$goCustomValues'");
							//                                                 array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");

							// 		}
							// 		$goHeaderOfCustomFields = implode(",", $goGetLastCustomFiledsName2);
							// 		$goCustomValues = implode(",", $goCustomValuesData);
							// 		$goCustomUpdate = implode(", ",  $goCustomUpdateData);
							// 		$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";

							// 		$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// 	}
						// }
						$goCountInsertedLeads++;
                        $apiresults = array("result" => "success", "message" => "$goCountInsertedLeads"); 
						# end set query for custom fields
					}//end if
					else{
						//merge only empty field

						$updatetData = array(
							'status' 			=> ($CheckDup[0]['status'] == '' 			? $status 			: $CheckDup[0]['status']),
							'vendor_lead_code' 	=> ($CheckDup[0]['vendor_lead_code'] == '' 	? $vendor_lead_code : $CheckDup[0]['vendor_lead_code']),
							'phone_code' 		=> ($CheckDup[0]['phone_code'] == '' 		? $phone_code 		: $CheckDup[0]['phone_code']),
							'phone_number' 		=> ($CheckDup[0]['phone_number'] == '' 		? $phone_number 	: $CheckDup[0]['phone_number']),
							'title' 			=> ($CheckDup[0]['title'] == '' 			? $title 			: $CheckDup[0]['title']),
							'first_name' 		=> ($CheckDup[0]['first_name'] == '' 		? $first_name 		: $CheckDup[0]['first_name']),
							'middle_initial' 	=> ($CheckDup[0]['middle_initial'] == '' 	? $middle_initial 	: $CheckDup[0]['middle_initial']),
							'last_name' 		=> ($CheckDup[0]['last_name'] == '' 		? $last_name 		: $CheckDup[0]['last_name']),
							'nric' 				=> ($CheckDup[0]['nric'] == '' 				? $nric 			: $CheckDup[0]['nric']),
							'race' 				=> ($CheckDup[0]['race'] == '' 				? $race 			: $CheckDup[0]['race']),
							'income' 			=> ($CheckDup[0]['income'] == '' 			? $income 			: $CheckDup[0]['income']),
							'address1' 			=> ($CheckDup[0]['address1'] == '' 			? $address1 		: $CheckDup[0]['address1']),
							'address2' 			=> ($CheckDup[0]['address2'] == '' 			? $address2 		: $CheckDup[0]['address2']),
							'address3' 			=> ($CheckDup[0]['address3'] == '' 			? $address3 		: $CheckDup[0]['address3']),
							'city' 				=> ($CheckDup[0]['city'] == '' 				? $city 			: $CheckDup[0]['city']),
							'state' 			=> ($CheckDup[0]['state'] == '' 			? $state 			: $CheckDup[0]['state']),
							'province' 			=> ($CheckDup[0]['province'] == '' 			? $province 		: $CheckDup[0]['province']),
							'postal_code' 		=> ($CheckDup[0]['postal_code'] == '' 		? $postal_code 		: $CheckDup[0]['postal_code']),
							'country_code' 		=> ($CheckDup[0]['country_code'] == '' 		? $country_code 	: $CheckDup[0]['country_code']),
							'gender' 			=> ($CheckDup[0]['gender'] == '' 			? $gender 			: $CheckDup[0]['gender']),
							'date_of_birth' 	=> ($CheckDup[0]['date_of_birth'] == '' 	? $date_of_birth 	: $CheckDup[0]['date_of_birth']),
							'alt_phone' 		=> ($CheckDup[0]['alt_phone'] == '' 		? $alt_phone 		: $CheckDup[0]['alt_phone']),
							'email' 			=> ($CheckDup[0]['email'] == '' 			? $email 			: $CheckDup[0]['email']),
							'security_phrase' 	=> ($CheckDup[0]['security_phrase'] == '' 	? $security_phrase 	: $CheckDup[0]['security_phrase']),
							'comments' 			=> ($CheckDup[0]['comments'] == '' 			? $comments 		: $CheckDup[0]['comments']),
						);
						// upload group list get gtoup name
						$tbl_name = 'group_list_' . $log_group;
						$astDB->where('phone_number', $phone_number);
						$rsltGoQueryInsDupList = $astDB->update($tbl_name, $updatetData);
						$duplicates++;
						$merge++;
						//$apiresults = array("result" => "error" , "message" => "Error: Lead File Contains Duplicates in List");
					}
				}
			} elseif($goDupcheck == "DUPLISTover") {
				#Duplicate check within the LIST
				if($goGetCheckcustomFieldNamesCorrect === "error" && empty($lead_mapping)) {
					fclose($handle);
				} else {					
					$tbl_name = 'group_list_' . $log_group;
					$astDB->where('phone_number', $phone_number);
					$CheckDup = $astDB->get($tbl_name, null, '*');
					$countResult = $astDB->getRowCount();
									
					////check in vicidial_dnc
					//$astDB->where('phone_number', $phone_number);
					//$resultCheckPhone = $astDB->getOne('vicidial_dnc', 'phone_number');
					//$countResult2 = $astDB->getRowCount();
						
					if($countResult < 1) {
						$USarea = substr($phone_number, 0, 3);
						$gmt_offset = lookup_gmt($astDB, $phone_code,$USarea,$state,$LOCAL_GMT_OFF_STD,$Shour,$Smin,$Ssec,$Smon,$Smday,$Syear,$postalgmt,$postal_code,$owner);
						
						//$goQueryInsDupList = "INSERT INTO vicidial_list (lead_id, entry_date, status, vendor_lead_code, list_id, gmt_offset_now, phone_code, phone_number, title, first_name, middle_initial, last_name, nric, race, income, address1, address2, address3, city, state, province, postal_code, country_code, gender, date_of_birth, alt_phone, email, security_phrase, comments, entry_list_id) VALUES ('', '$entry_date', '$status', '$vendor_lead_code', '$list_id', '$gmt_offset', '$phone_code', '$phone_number', '$title',	'$first_name', '$middle_initial', '$last_name', '$nric', '$race', '$income',	'$address1', '$address2', '$address3', '$city',	'$state', '$province', '$postal_code', '$country_code',	'$gender', '$date_of_birth', '$alt_phone', '$email', '$security_phrase', '$comments', '$entry_list_id');";
						$insertData = array(
							'lead_id' => '',
							'entry_date' => $entry_date,
							'status' => $status,
							'vendor_lead_code' => $vendor_lead_code,
							'list_id' => $list_id,
							'gmt_offset_now' => $gmt_offset,
							'phone_code' => $phone_code,
							'phone_number' => $phone_number,
							'title' => $title,
							'first_name' => $first_name,
							'middle_initial' => $middle_initial,
							'last_name' => $last_name,
							'nric' => $nric,
							'race' => $race,
							'income' => $income,
							'address1' => $address1,
							'address2' => $address2,
							'address3' => $address3,
							'city' => $city,
							'state' => $state,
							'province' => $province,
							'postal_code' => $postal_code,
							'country_code' => $country_code,
							'gender' => $gender,
							'date_of_birth' => $date_of_birth,
							'alt_phone' => $alt_phone,
							'email' => $email,
							'security_phrase' => $security_phrase,
							'comments' => $comments,
							'entry_list_id' => $entry_list_id,
							'last_local_call_time' => '0000-00-00 00:00:00'
						);
						// upload group list get gtoup name
						$tbl_name = 'group_list_' . $log_group;
						$rsltGoQueryInsDupList = $astDB->insert($tbl_name, $insertData);
						$goLastInsertedLeadIDDUPLIST = $astDB->getInsertId();
						$alex["insertquery"] = $astDB->getLastQuery();
						# start set query for custom fields
							// if(!empty($lead_mapping) && !empty($custom_array)){ // LEAD MAPPING CUSTOMIZATION
							// 	$goCustomKeyData = array();
							// 	$goCustomValuesData = array();
							// 	$goCustomUpdateData = array();

							// 	foreach($custom_array as $custom_key => $map_data){
							// 		$goCustomValues = $col[$map_data];
							// 		array_push($goCustomKeyData, "$custom_key");
							// 		array_push($goCustomValuesData, "'$goCustomValues'");
							// 		array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");

							// 		//$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_key) VALUES('$goLastInsertedLeadIDNODUP', '$goCustomValues') ON DUPLICATE KEY UPDATE $custom_key='$goCustomValues'";
							// 		//$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// 	}

							// 	$custom_keyValues = implode(",", $goCustomKeyData);
							// 	$goCustomValues = implode(",", $goCustomValuesData);
							// 	$goCustomUpdate = implode(", ",  $goCustomUpdateData);

							// 	$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_keyValues) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";
							// 	$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// }elseif($goCountTheHeader > 21) {
							// 	$goShowCustomFields = "DESC custom_$list_id;";
							// 	$rsltgoShowCustomFields = $astDB->rawQuery($goShowCustomFields);
							// 	$countResultrsltgoShowCustomFields = $astDB->getRowCount();
								
							// 	if($countResultrsltgoShowCustomFields > 1) {
							// 		$totalExplode = count($goGetLastHeader2);

							// 		$goCustomValuesData = array();
							//                                     $goCustomUpdateData = array();

							// 		for($ax=0; $ax < $totalExplode; $ax++) {
							// 			$goHeaderOfCustomFields = $goGetLastCustomFiledsName2[$ax]; #get the header name of the custom fields
							// 			$goCustomValues = $col[$goGetLastHeader2[$ax]]; #get the values of the custom fields
											
							// 			#$goQueryCustomFields = "INSERT INTO custom_$theList (lead_id,".$goHeaderOfCustomFields.") VALUES ('$goLastInsertedLeadIDDUPLIST','".$goCustomValues."');";
							// 			#$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', '$goCustomValues') ON DUPLICATE KEY UPDATE $goHeaderOfCustomFields='$goCustomValues'";
							// 			#$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
										
							// 			#$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads");
							// 			array_push($goCustomValuesData, "'$goCustomValues'");
							//                                                 array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");

							// 		}
							// 		$goHeaderOfCustomFields = implode(",", $goGetLastCustomFiledsName2);
							// 		$goCustomValues = implode(",", $goCustomValuesData);
							// 		$goCustomUpdate = implode(", ",  $goCustomUpdateData);
							// 		$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";

							// 		$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
							// 	}
						// }
						$goCountInsertedLeads++;
                        $apiresults = array("result" => "success", "message" => "$goCountInsertedLeads"); 
						# end set query for custom fields
					}//end if
					else{
						//merge only empty field

						$updatetData = array(
							'status' => $status,
							'vendor_lead_code' => $vendor_lead_code,
							'phone_code' => $phone_code,
							'phone_number' => $phone_number,
							'title' => $title,
							'first_name' => $first_name,
							'middle_initial' => $middle_initial,
							'last_name' => $last_name,
							'nric' => $nric,
							'race' => $race,
							'income' => $income,
							'address1' => $address1,
							'address2' => $address2,
							'address3' => $address3,
							'city' => $city,
							'state' => $state,
							'province' => $province,
							'postal_code' => $postal_code,
							'country_code' => $country_code,
							'gender' => $gender,
							'date_of_birth' => $date_of_birth,
							'alt_phone' => $alt_phone,
							'email' => $email,
							'security_phrase' => $security_phrase,
							'comments' => $comments,
						);
						// upload group list get gtoup name
						//error_log("name  585 -----> " . utf8_encode($first_name));
						$tbl_name = 'group_list_' . $log_group;
						$astDB->where('phone_number', $phone_number);
						$rsltGoQueryInsDupList = $astDB->update($tbl_name, $updatetData);
						
						// error_log("astDB->getLastQuery() 590 -----> " . $astDB->getLastQuery());
						$duplicates++;
						$over++;
						//$apiresults = array("result" => "error" , "message" => "Error: Lead File Contains Duplicates in List");
					}
				}
			} elseif($goDupcheck == "NONE") {
				#NO DUPLICATE CHECK
				if($goGetCheckcustomFieldNamesCorrect === "error" && empty($lead_mapping)) {
					fclose($handle);
				} else {
					$USarea = substr($phone_number, 0, 3);
					$gmt_offset = lookup_gmt($astDB, $phone_code,$USarea,$state,$LOCAL_GMT_OFF_STD,$Shour,$Smin,$Ssec,$Smon,$Smday,$Syear,$postalgmt,$postal_code,$owner);
			
					//$test_query = "INSERT INTO vicidial_list (lead_id, entry_date, status, vendor_lead_code, list_id, gmt_offset_now, phone_code, phone_number, title, first_name, middle_initial, last_name, nric, race, income, address1, address2, address3, city, state, province, postal_code, country_code, gender, date_of_birth, alt_phone, email, security_phrase, comments, entry_list_id, last_local_call_time) VALUES ('', '$entry_date', '$status', '$vendor_lead_code', '$list_id', '$gmt_offset', '$phone_code', '$phone_number', '$title',	'$first_name', '$middle_initial', '$last_name', '$nric', '$race', '$income',	'$address1', '$address2', '$address3', '$city',	'$state', '$province', '$postal_code', '$country_code',	'$gender', '$date_of_birth', '$alt_phone', '$email', '$security_phrase', '$comments', '$entry_list_id', '0000-00-00 00:00:00');";
					$insertData = array(
						'lead_id' => '',
						'entry_date' => $entry_date,
						'status' => $status,
						'vendor_lead_code' => $vendor_lead_code,
						'list_id' => $list_id,
						'gmt_offset_now' => $gmt_offset,
						'phone_code' => $phone_code,
						'phone_number' => $phone_number,
						'title' => $title,
						'first_name' => $first_name,
						'middle_initial' => $middle_initial,
						'last_name' => $last_name,
						'nric' => $nric,
						'race' => $race,
						'income' => $income,
						'address1' => $address1,
						'address2' => $address2,
						'address3' => $address3,
						'city' => $city,
						'state' => $state,
						'province' => $province,
						'postal_code' => $postal_code,
						'country_code' => $country_code,
						'gender' => $gender,
						'date_of_birth' => $date_of_birth,
						'alt_phone' => $alt_phone,
						'email' => $email,
						'security_phrase' => $security_phrase,
						'comments' => $comments,
						'entry_list_id' => $entry_list_id,
						'last_local_call_time' => '0000-00-00 00:00:00'
					);
					// upload group list get gtoup name
						$tbl_name = 'group_list_' . $log_group;
					$rsltGoQueryIns = $astDB->insert($tbl_name, $insertData);
					$goLastInsertedLeadIDNODUP = $astDB->getInsertId();
					// error_log('sql ----->>> ' . $astDB->getLastQuery() );
					// error_log('sql ----->>> ' . $astDB->getLastError() );
					$alex["query_insert"] = $astDB->getLastQuery();
					$alex["error_insert"] = $astDB->getLastError();
	
					# start set query for custom fields
						// if(!empty($lead_mapping) && !empty($custom_array)){ //LEAD MAPPING CUSTOMIZATION
						// 	$goCustomKeyData = array();
						// 	$goCustomValuesData = array();
						//                             $goCustomUpdateData = array();

						// 	foreach($custom_array as $custom_key => $map_data){
						// 		$goCustomValues = $col[$map_data];
						// 		array_push($goCustomKeyData, "$custom_key");
						// 		array_push($goCustomValuesData, "'$goCustomValues'");
						//         array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");

						// 		//$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_key) VALUES('$goLastInsertedLeadIDNODUP', '$goCustomValues') ON DUPLICATE KEY UPDATE $custom_key='$goCustomValues'";
						//         //$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
						// 	}

						// 	$custom_keyValues = implode(",", $goCustomKeyData);
						// 	$goCustomValues = implode(",", $goCustomValuesData);
						// 	$goCustomUpdate = implode(", ",  $goCustomUpdateData);
						
						// 	$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_keyValues) VALUES('$goLastInsertedLeadIDNODUP', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";
						//     $rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
						
						// }elseif($goCountTheHeader > 21) {
						// 	$goShowCustomFields = "DESC custom_$list_id;";
						// 	$rsltgoShowCustomFields = $astDB->rawQuery($goShowCustomFields);
						// 	$countResultrsltgoShowCustomFields = $astDB->getRowCount();
							
						// 	if($countResultrsltgoShowCustomFields > 1) {
						// 		$totalExplode = count($goGetLastHeader2);

						// 		$goCustomValuesData = array();
						// 		$goCustomUpdateData = array();

						// 		for($ax=0; $ax < $totalExplode; $ax++) {
						// 			$goHeaderOfCustomFields = $goGetLastCustomFiledsName2[$ax]; #get the header name of the custom fields
						// 			$goCustomValues = $col[$goGetLastHeader2[$ax]]; #get the values of the custom fields
										
						// 			#$rsltGoQueryCustomFields = mysqli_query($link, $goQueryCustomFields);
									
						// 			//$goQueryCustomFields .= "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDNODUP', '$goCustomValues') ON DUPLICATE KEY UPDATE $goHeaderOfCustomFields='$goCustomValues';";
						// 			//$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);

						// 			#$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads");

						// 			array_push($goCustomValuesData, "'$goCustomValues'");
						// 			array_push($goCustomUpdateData, "$goHeaderOfCustomFields='$goCustomValues'");

						// 		}

						// 		$goHeaderOfCustomFields = implode(",", $goGetLastCustomFiledsName2);
						// 		$goCustomValues = implode(",", $goCustomValuesData);
						// 		$goCustomUpdate = implode(", ",  $goCustomUpdateData);
						// 		$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDNODUP', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";

						// 		$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
						// 	} 	
					// }
					
					$goCountInsertedLeads++;
					$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads", "alex_data" => $alex);

				# end set query for custom fields
				}
			} else{
					#Ingnore Duplicate check within the LIST
					if($goGetCheckcustomFieldNamesCorrect === "error" && empty($lead_mapping)) {
						fclose($handle);
					} else {
						//$goQueryCheckDupPhone = "SELECT phone_number FROM vicidial_list WHERE phone_number='$phone_number' AND list_id='$list_id';";
						$astDB->where('phone_number', $phone_number);
						// upload group list get gtoup name
						$tbl_name = 'group_list_' . $log_group;
						$rsltCheckDupPhone = $astDB->get($tbl_name, null, 'phone_number');
						$countResult = $astDB->getRowCount();
						
						if($countResult < 1) {
							$USarea = substr($phone_number, 0, 3);
							$gmt_offset = lookup_gmt($astDB, $phone_code,$USarea,$state,$LOCAL_GMT_OFF_STD,$Shour,$Smin,$Ssec,$Smon,$Smday,$Syear,$postalgmt,$postal_code,$owner);
							
							//$goQueryInsDupList = "INSERT INTO vicidial_list (lead_id, entry_date, status, vendor_lead_code, list_id, gmt_offset_now, phone_code, phone_number, title, first_name, middle_initial, last_name, nric, race, income, address1, address2, address3, city, state, province, postal_code, country_code, gender, date_of_birth, alt_phone, email, security_phrase, comments, entry_list_id) VALUES ('', '$entry_date', '$status', '$vendor_lead_code', '$list_id', '$gmt_offset', '$phone_code', '$phone_number', '$title',	'$first_name', '$middle_initial', '$last_name', '$nric', '$race', '$income',	'$address1', '$address2', '$address3', '$city',	'$state', '$province', '$postal_code', '$country_code',	'$gender', '$date_of_birth', '$alt_phone', '$email', '$security_phrase', '$comments', '$entry_list_id');";
							$insertData = array(
								'lead_id' => '',
								'entry_date' => $entry_date,
								'status' => $status,
								'vendor_lead_code' => $vendor_lead_code,
								'list_id' => $list_id,
								'gmt_offset_now' => $gmt_offset,
								'phone_code' => $phone_code,
								'phone_number' => $phone_number,
								'title' => $title,
								'first_name' => $first_name,
								'middle_initial' => $middle_initial,
								'last_name' => $last_name,
								'nric' => $nric,
								'race' => $race,
								'income' => $income,
								'address1' => $address1,
								'address2' => $address2,
								'address3' => $address3,
								'city' => $city,
								'state' => $state,
								'province' => $province,
								'postal_code' => $postal_code,
								'country_code' => $country_code,
								'gender' => $gender,
								'date_of_birth' => $date_of_birth,
								'alt_phone' => $alt_phone,
								'email' => $email,
								'security_phrase' => $security_phrase,
								'comments' => $comments,
								'entry_list_id' => $entry_list_id,
								'last_local_call_time' => '0000-00-00 00:00:00'
							);
							// upload group list get gtoup name
							$tbl_name = 'group_list_' . $log_group;
							$rsltGoQueryInsDupList = $astDB->insert($tbl_name, $insertData);
							$goLastInsertedLeadIDDUPLIST = $astDB->getInsertId();
							$alex["insertquery"] = $astDB->getLastQuery();
							# start set query for custom fields
								// if(!empty($lead_mapping) && !empty($custom_array)){ // LEAD MAPPING CUSTOMIZATION
								// 	$goCustomKeyData = array();
								// 	$goCustomValuesData = array();
								// 	$goCustomUpdateData = array();
		
								// 	foreach($custom_array as $custom_key => $map_data){
								// 		$goCustomValues = $col[$map_data];
								// 		array_push($goCustomKeyData, "$custom_key");
								// 		array_push($goCustomValuesData, "'$goCustomValues'");
								// 		array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");
		
								// 		//$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_key) VALUES('$goLastInsertedLeadIDNODUP', '$goCustomValues') ON DUPLICATE KEY UPDATE $custom_key='$goCustomValues'";
								// 		//$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
								// 	}
		
								// 	$custom_keyValues = implode(",", $goCustomKeyData);
								// 	$goCustomValues = implode(",", $goCustomValuesData);
								// 	$goCustomUpdate = implode(", ",  $goCustomUpdateData);
		
								// 	$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $custom_keyValues) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";
								// 	$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
								// }elseif($goCountTheHeader > 21) {
								// 	$goShowCustomFields = "DESC custom_$list_id;";
								// 	$rsltgoShowCustomFields = $astDB->rawQuery($goShowCustomFields);
								// 	$countResultrsltgoShowCustomFields = $astDB->getRowCount();
									
								// 	if($countResultrsltgoShowCustomFields > 1) {
								// 		$totalExplode = count($goGetLastHeader2);
		
								// 		$goCustomValuesData = array();
								//                                     $goCustomUpdateData = array();
		
								// 		for($ax=0; $ax < $totalExplode; $ax++) {
								// 			$goHeaderOfCustomFields = $goGetLastCustomFiledsName2[$ax]; #get the header name of the custom fields
								// 			$goCustomValues = $col[$goGetLastHeader2[$ax]]; #get the values of the custom fields
												
								// 			#$goQueryCustomFields = "INSERT INTO custom_$theList (lead_id,".$goHeaderOfCustomFields.") VALUES ('$goLastInsertedLeadIDDUPLIST','".$goCustomValues."');";
								// 			#$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', '$goCustomValues') ON DUPLICATE KEY UPDATE $goHeaderOfCustomFields='$goCustomValues'";
								// 			#$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
											
								// 			#$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads");
								// 			array_push($goCustomValuesData, "'$goCustomValues'");
								//                                                 array_push($goCustomUpdateData, "$custom_key='$goCustomValues'");
		
								// 		}
								// 		$goHeaderOfCustomFields = implode(",", $goGetLastCustomFiledsName2);
								// 		$goCustomValues = implode(",", $goCustomValuesData);
								// 		$goCustomUpdate = implode(", ",  $goCustomUpdateData);
								// 		$goQueryCustomFields = "INSERT INTO custom_$theList(lead_id, $goHeaderOfCustomFields) VALUES('$goLastInsertedLeadIDDUPLIST', $goCustomValues) ON DUPLICATE KEY UPDATE $goCustomUpdate";
		
								// 		$rsltGoQueryCustomFields = $astDB->rawQuery($goQueryCustomFields);
								// 	}
							// }
							$goCountInsertedLeads++;
							$apiresults = array("result" => "success", "message" => "$goCountInsertedLeads"); 
							# end set query for custom fields
						}//end if
						else{
							//fclose($handle);
							$duplicates++;
							//$apiresults = array("result" => "error" , "message" => "Error: Lead File Contains Duplicates in List");
						}
					}
			}
			//fclose($handle);
			$counter++;
			if($counter == $every1k){

				$updata = 'upload counter -->> counter = ' . $counter .' | goCountInsertedLeads = '.$goCountInsertedLeads .' | merge = '.$merge .' | over = '.$over .' | duplicates = '.$duplicates.'';
				
				writedata($updata);
					
				error_log('upload counter -->> ' . $counter .' - '.$goCountInsertedLeads .' - '.$merge .' - '.$over .' - '.$duplicates);
				
				$every1k = $every1k + 1000;
			}
		} #end while
		error_log('list upload counter finish counter-->> ' . $counter .' - '.$goCountInsertedLeads .' - '.$merge .' - '.$over .' - '.$duplicates);
	
		fclose($handle);
		if($goCountInsertedLeads > 0 && $duplicates < 1) {
			$apiresults = array("result" => "success", "message" => "Total Uploaded Leads: $goCountInsertedLeads" , "alex_data" => $alex);
		}elseif($goCountInsertedLeads > 0 && $duplicates > 0){
			if($merge > 0){
				$apiresults = array("result" => "success", "message" => "Uploaded:$goCountInsertedLeads    Merged:$merge");
			}elseif($over > 0){
				$apiresults = array("result" => "success", "message" => "Uploaded:$goCountInsertedLeads    Overwrite:$over");
			}else{
				$apiresults = array("result" => "success", "message" => "Uploaded:$goCountInsertedLeads    Duplicates:$duplicates");
			}
		}elseif($goGetCheckcustomFieldNamesCorrect === "error"){
			$apiresults = array("result" => "error" , "message" => "Error: Lead File Not Compatible with List. Incompatible Field Names. Check the File Headers");
		}elseif($duplicates > 0){
			if($merge > 0){
				$apiresults = array("result" => "success" , "message" => "merged : $duplicates");
			}elseif($over > 0){
				$apiresults = array("result" => "success" , "message" => "Overwrite : $duplicates");
			}else{
				$apiresults = array("result" => "success" , "message" => "Duplicates : $duplicates");
			}
		}else {
			$apiresults = array("result" => "error", "message" => "$goCountInsertedLeads", "duplicates" => $duplicates, "alex_data" => $alex);
		}
		$log_id = log_action($astDB, 'UPLOAD', $log_user, $log_ip, "Uploaded {$goCountInsertedLeads} leads on List ID $theList", $log_group);

		//update group_list_status
		// $updatetData = array(
		// 	'status' => "finish"
		// );
		// $astDB->where('glsid', $update_use_last_id);
		// $rsltGoQueryInsDupList = $astDB->update("group_list_status", $updatetData);

		//if more than 2000 list , possible the system throw 503 to php page
		//so we send some msg to user and system will keep process in background
		if($counter >2000){
			// message parameters	

			$finish = 'finish counter -->> counter = ' . $counter .' | goCountInsertedLeads = '.$goCountInsertedLeads .' | merge = '.$merge .' | over = '.$over .' | duplicates = '.$duplicates.'';
			
			writedata($finish);
				
			error_log($finish);
			//got show until here, but not below
			//move this to top and let system get the to and from user id 1st,

			// $astDB->where('user', $log_user);
			// $touserid = $astDB->getOne("vicidial_users", 'user_id');
			// //get system user id, cos not all user system are same id
			// $astDB->where('user', "system");
			// $fromuserid = $astDB->getOne("vicidial_users", 'user_id');
		
		
			//get user id
			$subject = "Lead Uploaded"; //
			//$message = "date :" . $start_date . "   <br>   Uploaded : " . $counter . " <br>  , Merged : " . $merge . "   <br>   , Overwrite : " . $over . "   <br>  , Duplicates : " . $duplicates."  <br>  click <a href='/crm3.php'>here</a> Lead page <br> <b>This is an auto generated email, Do not reply  </b>  ";
			
			$message = " Uploaded : " . $counter . ", <br> New : ".$goCountInsertedLeads.", <br>   Merged : " . $merge . ", <br>   Overwrite : " . $over . ", <br>   Duplicates : " . $duplicates."";
			$message = "$message";
			$external_recipients = null;

			//unable to call dbHandaller, use this 1st, if someone read this and know how to do, pls change to call dbhandaler $db->sendMessage();
			// PHP Fatal error:  Cannot declare class MysqliDb, because the name is already in use in /var/www/html/php/db_connectors/MysqliDb.php on line 18
			
			// Now store the message in our database.
			// try to store the inbox message for the target user. Start transaction because we could have attachments.
			//
			//change this to proper insert data and c how it goes
			$date = date("Y-m-d H:i:s");
			$insertData = array(
				'user_from' => $fromusrid,
				'user_to' => $tousrid,
				'subject' => $subject,
				'message' => $message,
				'date' => $date,
				'message_read' => '0',
				'favorite' => '0'
			);
			// upload group list get gtoup name
			$goDB->insert('messages_inbox', $insertData);
			error_log('sql ---> ' . $goDB->getLastQuery());
			

			$finish = 'sql ---> ' . $goDB->getLastQuery().'';
			writedata($finish);

			//chk the message insert or not
			$goDB->where('date', $date);
			$chkdate = $goDB->getOne("messages_inbox", 'date');
			$chkdatess = $chkdate["date"];
			
			if(count($chkdate)<1){
				error_log('insert failed ---> ');
				error_log('sql err ---> ' . $goDB->getLastError());
				
				$err = 'sql err---> ' . $goDB->getLastError().'';
				writedata($err);

				$goDB->insert('messages_inbox', $insertData);
				error_log('sql ---> ' . $goDB->getLastQuery());
				
				$finish = 'sql ---> ' . $goDB->getLastQuery().'';
				
				writedata($finish);
			}else{
				error_log('insert scuesses ---> ');
				writedata('insert scuesses');
			}
			// $query = "INSERT INTO goautodial.messages_inbox (user_from,user_to,subject,message,date,message_read,favorite) ";
			// $query .= "values ('".$fromuserid["user_id"]."','".$touserid["user_id"]."','".$subject."','".$message."','".$date."','0','0')";
			// $goDB->rawQuery($query);
			// $inboxmsgid = $goDB->getInsertId();

			//temp no need cos not sure this is nessearry or not
			// $query = "INSERT INTO goautodial.messages_outbox (user_from,user_to,subject,message,date,message_read,favorite) ";
			// $query .= "values ('".$fromuserid["user_id"]."','".$touserid["user_id"]."','".$subject."','".$message."','".$date."','1','0')";
			// $goDB->rawQuery($query);

			// $query = "INSERT INTO goautodial.timeline (type_id,type,user_from_id,user_from_id,title,description,start_date) ";
			// $query .= "values ('".$inboxmsgid."','message','".$touserid["user_id"]."','".$fromuserid["user_id"]."','".$subject."','".$message."','".$date."')";
			// $goDB->rawQuery($query);
				
			$log_id = log_action($astDB, 'UPLOAD', $log_user, $log_ip, "Uploaded {$goCountInsertedLeads} leads on List ID $theList", $log_group);
			
		}
		
	} // END IF handle

	function writedata($str){
		$date = date("Y-m-d H:i:s");
		file_put_contents('/var/log/updata', PHP_EOL .'['.$date.'] - '.$str, FILE_APPEND);
	}

	function goGetCampaignList($link, $goCampaignID) {
		//$goCheckCamp = "SELECT list_id FROM vicidial_lists WHERE campaign_id='$goCampaignID';";
		$link->where('campaign_id', $goCampaignID);
		$rsltgoCheckCamp = $link->get('vicidial_lists', null, 'list_id');
		$countResultCamp = $link->getRowCount();
		$goDUPLists = '';
		foreach ($rsltgoCheckCamp as $fresultsDup) {
			$goDUPLists .= $fresultsDup['list_id'].",";
		}
		return $goDUPLists;
	}
	
	function goCheckCustomFieldsName($link, $goCClistID, $gocustomFieldsCSV) {
		// check fieldnames are correct
		//$goSQLCheckFieldsCustom = "SELECT $gocustomFieldsCSV FROM custom_$goCClistID;";

		$goCustomCheckQuery = "SELECT EXISTS(SELECT $gocustomFieldsCSV FROM custom_$goCClistID)";
		$customCheck = $link->rawQuery($goCustomCheckQuery);
		$countCustomCheck = $link->getRowCount();

		if( $countCustomCheck === 0 ){
			return "error";
		}

		$rsltSQLCHECK = $link->get("custom_$goCClistID", null, "$gocustomFieldsCSV");
		
		if(!$rsltSQLCHECK){
			$goRetMessage = "error";
		} else {
			/*$goShowCustomFields = "DESC custom_$goCClistID;";
			$rsltgoShowCustomFields = $link->rawQuery($goShowCustomFields);
			$countResultrsltgoShowCustomFields = $link->getRowCount();
				
			if($countResultrsltgoShowCustomFields > 1) {
				foreach ($rsltgoShowCustomFields as $fresultsShow){
					$goCustomFields .= $fresultsShow['Field'].",";
				}
				
				$goRetMessage = preg_replace("/,$/",'',$goCustomFields);
			}*/
			$goRetMessage = "success";
		}
				
		return $goRetMessage;
	}
	
	// check 1st if fields are not less than 21
	// check 2nd if greater than 21 check the field name spelling from csv vs on the DB custom_LISTID
	// lookup_gmt extgetval
	// $field_regx = "/['\"`\\;]/";
	// $vendor_lead_code =             preg_match($field_regx, "", $vendor_lead_code);
	// $vendor_lead_code =             preg_replace($field_regx, "", $vendor_lead_code);

//echo "File data successfully imported to database!!";
?>
