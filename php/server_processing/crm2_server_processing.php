<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

define('__ROOT__', dirname(dirname(__FILE__)));
// include( __ROOT__.'/Session.php');
// DB table to use
// $table = 'vicidial_list';
$table = <<<EOT
	(
		SELECT vl.lead_id,vl.list_id,vl.first_name,vl.middle_initial,vl.last_name,vl.status,vl.phone_number,vl.last_local_call_time,vc.callback_time,vl.nric,vl.title,vl.address1,vl.postal_code 
		FROM vicidial_list vl 
		left join (select lead_id, callback_time from vicidial_callbacks where status !='INACTIVE') vc on vl.lead_id = vc.lead_id 
		where 1
		AND vl.status not in ( 'DNC', 'DNCL' ) 
	) temp
EOT;

$list_id 											= $_GET['list_id'];
// error_log('list_id ------> ' . $list_id);

// Table's primary key
$primaryKey = 'lead_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'lead_id', 				'dt' => 0 ),
	array( 'db' => 'first_name',  			'dt' => 1 ),
	array( 'db' => 'middle_initial',  		'dt' => 2 ),
	array( 'db' => 'last_name',  			'dt' => 3 ),
	array( 'db' => 'status',  				'dt' => 4 ),
	array( 'db' => 'phone_number',   		'dt' => 5 ),
	array( 'db' => 'last_local_call_time',  'dt' => 6 ),
	array( 'db' => 'callback_time',     	'dt' => 7 ),
	// array( 'db' => 'nric',     				'dt' => 8 ),
	array( 'db' => 'title',    				'dt' => 8 ),
	array( 'db' => 'address1',     			'dt' => 9 ),
	array( 'db' => 'postal_code',     		'dt' => 10 )
);

$ip_server = $_SERVER['SERVER_ADDR']; 
// SQL server connection information
$sql_details = array(
	'user' => 'client',
	'pass' => 'vicidialnow',
	'db'   => 'asterisk',
	'host' => $ip_server,
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
// $encoded = json_encode($value, DEFINED('JSON_INVALID_UTF8_IGNORE') ? JSON_INVALID_UTF8_IGNORE : 0);

require( __ROOT__.'/ssp.class.php' );

$json = json_encode(
	SSP::lead( $_GET, $sql_details, $table, $primaryKey, $columns, $list_id), DEFINED('JSON_INVALID_UTF8_IGNORE') ? JSON_INVALID_UTF8_IGNORE : 0 
	);

// error_log('lists lists lists--->> '.$lists);
if ($json)
    echo $json;
else
	error_log('error json --->>> ' .  json_last_error_msg());
	


