<?php
 /**
 * @file 		goGetCallbackCountSeldate.php
 * @brief 		API for Agent UI
 * @copyright 	Copyright (C) GOautodial Inc.
 * @author     	Chris Lomuntad <chris@goautodial.com>
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//$is_logged_in = check_agent_login($astDB, $goUser);
//
//$agent = get_settings('user', $astDB, $goUser);
$settings = get_settings('system', $astDB);
//$user_group = $agent->user_group;

if (isset($_GET['goServerIP'])) { $server_ip = $astDB->escape($_GET['goServerIP']); }
    else if (isset($_POST['goServerIP'])) { $server_ip = $astDB->escape($_POST['goServerIP']); }
if (isset($_GET['goSessionName'])) { $session_name = $astDB->escape($_GET['goSessionName']); }
    else if (isset($_POST['goSessionName'])) { $session_name = $astDB->escape($_POST['goSessionName']); }
if (isset($_GET['goUserID'])) { $user_id = $astDB->escape($_GET['goUserID']); }
    else if (isset($_POST['goUserID'])) { $user_id = $astDB->escape($_POST['goUserID']); }

if (isset($_GET['goCampaign'])) { $goCampaign = $astDB->escape($_GET['goCampaign']); }
    else if (isset($_POST['goCampaign'])) { $goCampaign = $astDB->escape($_POST['goCampaign']); }
if (isset($_GET['selDate'])) { $selDate = $astDB->escape($_GET['selDate']); }
	else if (isset($_POST['selDate'])) { $selDate = $astDB->escape($_POST['selDate']); }


$user = (strlen($user_id) > 0) ? $user_id : $goUser;

// Callbacks Today
$stmt = "SELECT * FROM vicidial_callbacks WHERE recipient='USERONLY' AND user='$user' AND status NOT IN('INACTIVE','DEAD') AND callback_time BETWEEN '$selDate 00:00:00' AND '$selDate 23:59:59'AND lead_status != 'W' ORDER BY callback_time ASC;";
$rslt = $astDB->rawQuery($stmt);
$cbcount_today = $astDB->getRowCount();

// Callbacks Today
$stmt = "SELECT * FROM vicidial_callbacks WHERE recipient='USERONLY' AND user='$user' AND status NOT IN('INACTIVE','DEAD') AND callback_time BETWEEN '$selDate 00:00:00' AND '$selDate 23:59:59' AND lead_status = 'W' ORDER BY callback_time ASC;";
$rslt = $astDB->rawQuery($stmt);
$cbcount_today_w = $astDB->getRowCount();


// error_log('sql cb date cnt ---> ' . $stmt);

//echo "$cbcount|$cbcount_live|$cbcount_today";
$APIResult = array("callback_today" => $cbcount_today ,"callback_today_w" => $cbcount_today_w ,"goCampaign" => $goCampaign ,"selDate" => $selDate );
?>