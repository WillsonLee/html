<?php

/**
 * @file 		crm.php
 * @brief 		Manage leads and contacts
 * @copyright 	Copyright (c) 202 GOautodial Inc. 
 * @author		Demian Lizandro A. Biscocho
 * @author     	Alexander Jim H. Abenoja
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

require_once('./php/UIHandler.php');
require_once('./php/APIHandler.php');
require_once('./php/CRMDefaults.php');
require_once('./php/LanguageHandler.php');
include('./php/Session.php');

$ui = \creamy\UIHandler::getInstance();
$api = \creamy\APIHandler::getInstance();
$lh = \creamy\LanguageHandler::getInstance();
$user = \creamy\CreamyUser::currentUser();

//proper user redirects
if ($user->getUserRole() != CRM_DEFAULTS_USER_ROLE_ADMIN) {
	if ($user->getUserRole() == CRM_DEFAULTS_USER_ROLE_AGENT) {
		header("location: agent.php");
	}
}

// cancel confirm form resubmission on back button php 
// cos this page dont need special sudmit, just normal view 
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");

?>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php $lh->translateText('portal_title'); ?> - <?php $lh->translateText("crm"); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<?php
	print $ui->standardizedThemeCSS();
	print $ui->creamyThemeCSS();
	print $ui->dataTablesTheme();
	?>

	<!-- Datetime picker -->
	<link rel="stylesheet" href="js/dashboard/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

	<!-- Date Picker -->
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/moment.js"></script>
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>


	<!-- BEGIN willson crm -->
	<link rel="stylesheet" href="DataTables/datatables.min.css">
	<!-- END willson crm -->

	<!-- CHOSEN-->
	<link rel="stylesheet" src="js/dashboard/chosen_v1.2.0/chosen.min.css">
</head>
<?php print $ui->creamyBody(); ?>
<div class="wrapper">
	<!-- header logo: style can be found in header.less -->
	<?php print $ui->creamyHeader($user); ?>
	<!-- Left side column. contains the logo and sidebar -->
	<?php print $ui->getSidebar($user->getUserId(), $user->getUserName(), $user->getUserRole(), $user->getUserAvatar(), $_SESSION['usergroup']); ?>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header content-heading">
			<h1>
				<?php $lh->translateText("crm_title"); ?>
				<small><?php $lh->translateText("crm"); ?></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="./index.php"><i class="fa fa-phone"></i> <?php $lh->translateText("home"); ?></a></li>
				<li class="active"><?php $lh->translateText("crm_title"); ?>
			</ol>
		</section>
		
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-lg-12">
					<!-- <form id="search_form"> -->
					<form action="crm2.php" method="post">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="contacts_div">
									<table class="display" id="tbllist"  style="width:100%">
										<thead>
											<tr>
												<th>Lead ID</th>
												<th>Full Name</th>
												<th>Middle Name</th>
												<th>Last Name</th>
												<th>Status</th>
												<th>Phone Number</th>
												<th>Last Call</th>
												<th>Call Back</th>
												<!-- <th>NRIC</th> -->
												<th>Title</th>
												<th>Address</th>
												<th>Postal Code</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Lead ID</th>
												<th>Full Name</th>
												<th>Middle Name</th>
												<th>Last Name</th>
												<th>status</th>
												<th>Phone Number</th>
												<th>Last Call</th>
												<th>Call Back</th>
												<!-- <th>NRIC</th> -->
												<th>Title</th>
												<th>Address</th>
												<th>Postal Code</th>
											</tr>
										</tfoot>
										
									</table>
								</div>
							</div><!-- /.body -->
						</div><!-- /.panel -->
					</form>
				</div><!-- /.col-lg-12 -->
			</div><!-- /. row -->
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
	<?php print $ui->getRightSidebar($user->getUserId(), $user->getUserName(), $user->getUserAvatar()); ?>
</div><!-- ./wrapper -->

<?php print $ui->standardizedThemeJS(); ?>

<!-- CHOSEN-->
<script src="js/dashboard/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<!-- BEGIN willson crm js -->
<script type="text/javascript" src="DataTables/Select-1.3.1/js/select.dataTables.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<!-- END willson crm js -->

<script type="text/javascript">
var id = <?php echo json_encode($_POST['list_id'] ?? null) ?>;
	$(document).ready(function() {
		// Setup - add a text input to each thead cell
		(function(next) {
			//console.log('1st');
			inittbl();
			next()
		}(function() {
			//console.log('2nd');
			setTimeout(
			function() 
			{
				callafunc();
			}, 1000);
		}))
		
		$(document).on('click','.buttons-csv',function() {
			// console.log('clicked csv search = ' + $("#ssearch").val());
			// console.log('asc --> ' + $('.sorting_asc').text());
			// console.log('desc --> ' + $('.sorting_desc').text());
			// console.log('custom --> ' + $('.custom-select option:selected').text());
			// console.log('offset --> ' + $('.paginate_button.page-item.active').find(".page-link").text());
			var lead_id =   $('#tbllist td:nth-child(1)').map(function() {
							return $(this).text();
						}).get();
			// console.log('table --> ' + lead_id);

			var data = {
				"api": 'crm2',
				// "searchkey"	: $("#ssearch").val(),
				// "limit"		: $('.custom-select option:selected').text(),
				// "asc"		: $('.sorting_asc').text(),
				// "desc"		: $('.sorting_desc').text(),
				// "paging"	: $('.paginate_button.page-item.active').find(".page-link").text(),
				"lead_id" 	: lead_id
			};
			$.post("/php/apijs/apijs.php", data);
		});

		function callafunc(){
			//console.log('has class ' + $(".odd").children().hasClass("dataTables_empty"));
			if($(".odd").children().hasClass("dataTables_empty")){
				
				//$('#tbllist').DataTable().page.len(10).draw();
				$('#tbllist').DataTable().state.clear();
				$('#tbllist').DataTable().ajax.reload(null, true);
			}
		}
	});


	function inittbl(){

		$('#tbllist thead th').each( function () {
        	var title = $(this).text();
        	$(this).html( title + '<input type="text"  style="width:100%" />' );
		} );
		/**	for dom selection meaning
			B - Button
			l - length changing input control
			f - filtering input
			t - The table!
			i - Table information summary
			p - pagination control
			r - processing display element
		*/
		var table = $('#tbllist').DataTable( {
			"dom"			: '<"top"Blp>rt<"bottom"i><"clear">',
			"serverSide"	: true,
        	"select"		: true,
			"stateSave"		: true,
        	"responsive"	: true,
			"pagingType"	: "full_numbers",
			"aLengthMenu"	: [[10, 50, 100, 500, 1000, 2000, 5000, 10000], [10, 50, 100, 500, 1000, 2000, 5000, 10000]],
			"ajax"			: './php/server_processing/crm2_server_processing.php?list_id="'+id+'"' ,
			"buttons"		: [
				'csv',
				{
					text: 'View',
					action: function () {
						var url = './editcontacts.php';
						var id = table.rows( { selected: true } ).data()[0][0];
						var form = $('<form action="' + url + '" method="post"><input type="hidden" name="modifyid" value="'+id+'" /></form>');
						$('body').append(form);  // This line is not necessary
						$(form).submit();

						// console.log('selected data -> ' + table.rows( { selected: true } ).data()[0][0]);
						// console.log('selected -> ' + table.rows( { selected: true })[0][0]);
						// console.log('selected array -> ' + table.rows( { selected: true } ).data().toArray()[0]);
						
					}
				}
				,{
					text: 'delete',
					action: function () {
						//var url = './editcontacts2.php';
						var id = table.rows( { selected: true } ).data().toArray()	;
						var led_id;
						//console.log('modify count id = ' + table.rows( { selected: true } ).data().length);
						
						for (i = 0; i < table.rows( { selected: true } ).data().length; i++) {
							//led_id.push(id[i][0]) ;
							if(i == '0'){
								led_id = id[i][0];
							}else{
								led_id += ',' +id[i][0];
							}
						}
						swal({
							title: "<?php $lh->translateText("are_you_sure"); ?>",
							text: "<?php $lh->translateText("action_cannot_be_undone"); ?>",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "<?php $lh->translateText("confirm_delete_contact"); ?>",
							cancelButtonText: "<?php $lh->translateText("cancel_please"); ?>",
							closeOnConfirm: false,
							closeOnCancel: false
							},
							function(isConfirm){
								if (isConfirm) {
									$.ajax({
										url: "./php/crm/DeleteContact.php",
											type: 'POST',
											data: {
												leadid: led_id
											},
											success: function(data) {
												if (data == 1) {
													swal({
														title: "<?php $lh->translateText("success"); ?>",
														text: "<?php $lh->translateText("contact_delete_success"); ?>",
														type: "success"
														},
														function(){
															// window.location.href = 'crm2.php';
															// location.reload();
															$('#tbllist').DataTable().ajax.reload(null, false);
														}
													);
												} else {
													sweetAlert("<?php $lh->translateText("oups"); ?>", "<?php $lh->translateText("something_went_wrong"); ?>"+data, "error");
													window.setTimeout(function(){$('#delete_notification_modal').modal('hide');}, 3000);
												}
											}
									});
								} else {
										swal("<?php $lh->translateText("cancelled"); ?>", "<?php $lh->translateText("cancel_message"); ?>", "error");
								}
							}
						);//end of confirm delete
					}
				}
				// ,{
					// 	text: 'test',
					// 	action: function () {
							

					// 	//var table = $('#example').DataTable();
					// 	var info = table.page.info();

					// 	console.log('Currently showing page '+(info.page+1)+' of '+info.pages+' pages.');
					// 	console.log('info = ' + JSON.stringify(info));
						
					// 	}
				// }
			],
			// search by indi col
			"initComplete"	: function () {
				// Apply the search
				this.api().columns().every( function () {
					var that = this;
	
					$( 'input', this.header() ).on( 'keyup change clear', function () {
						if ( that.search() !== this.value ) {
							that
								.search( this.value )
								.draw();
						}
					} );
				} );
			},
			"columnDefs": [
				{
					// The `data` parameter refers to the data for the cell (defined by the
					// `data` option, which defaults to the column being worked with, in
					// this case `data: 1`.
					"render": function ( data, type, row ) {
						row[2] == null ? mid = '' : mid = row[2];
						row[3] == null ? last = '' : last = row[3];

						return data + ' '+ mid + ' ' + ' '+ last;
					},
					"targets": 1
				},
				{ "visible": false,  "targets": [ 2 ] },
				{ "visible": false,  "targets": [ 3 ] }
			]
		});
	}
</script>




</body>

</html>