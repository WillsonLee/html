<?php

/**
 * @file 		crm.php
 * @brief 		Manage leads and contacts
 * @copyright 	Copyright (c) 202 GOautodial Inc. 
 * @author		Demian Lizandro A. Biscocho
 * @author     	Alexander Jim H. Abenoja
 *
 * @par <b>License</b>:
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

require_once('./php/UIHandler.php');
require_once('./php/APIHandler.php');
require_once('./php/CRMDefaults.php');
require_once('./php/LanguageHandler.php');
include('./php/Session.php');

$ui = \creamy\UIHandler::getInstance();
$api = \creamy\APIHandler::getInstance();
$lh = \creamy\LanguageHandler::getInstance();
$user = \creamy\CreamyUser::currentUser();

//proper user redirects
if ($user->getUserRole() != CRM_DEFAULTS_USER_ROLE_ADMIN) {
	if ($user->getUserRole() == CRM_DEFAULTS_USER_ROLE_AGENT) {
		header("location: agent.php");
	}
}
?>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php $lh->translateText('portal_title'); ?> - <?php $lh->translateText("lead_log"); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<?php
	print $ui->standardizedThemeCSS();
	print $ui->creamyThemeCSS();
	print $ui->dataTablesTheme();
	?>

	<!-- Datetime picker -->
	<link rel="stylesheet" href="js/dashboard/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

	<!-- Date Picker -->
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/moment.js"></script>
	<script type="text/javascript" src="js/dashboard/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<!-- BEGIN willson crm -->
	<link rel="stylesheet" href="DataTables/datatables.min.css">
	<!-- END willson crm -->

	<!-- CHOSEN-->
	<link rel="stylesheet" src="js/dashboard/chosen_v1.2.0/chosen.min.css">
</head>
<?php print $ui->creamyBody(); ?>
<div class="wrapper">
	<!-- header logo: style can be found in header.less -->
	<?php print $ui->creamyHeader($user); ?>
	<!-- Left side column. contains the logo and sidebar -->
	<?php print $ui->getSidebar($user->getUserId(), $user->getUserName(), $user->getUserRole(), $user->getUserAvatar(), $_SESSION['usergroup']); ?>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php $lh->translateText("chglog"); ?>
				<!-- <small><?php $lh->translateText("GOautodial_team"); ?></small> -->
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-list-alt"></i> <?php $lh->translateText("home"); ?></a></li>
				<li class="active"><?php $lh->translateText("chglog"); ?></li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">

			<div id='adminuse' <?php if($_SESSION['usergroup'] != 'ADMIN') { echo 'style="display: none;"';} ?> class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-2">
									<input id='txtchgid' style="display: none;" type='text' style="width:100%">
									Version :
									<input id='txtversion' type='text' style="width:100%">
								</div>
								<div class="col-lg-4">
									Feature : 
									<input id='txtfeature' type='text' style="width:100%">
								</div>
								<div class="col-lg-2">
									Editor : 
									<input id='txteditor' type='text' style="width:100%">
								</div>
								<div class="col-lg-2">
									Date : 
									<?php 
										$month = date('m');
										$day = date('d');
										$year = date('Y');
										$today = $year . '-' . $month . '-' . $day;
									?>
									<input id='txtdate' type='date' value="<?php echo $today; ?>" style="width:100%">
								</div>
								<div class="col-lg-2">
									<!-- <button id='savelog' onclick="save_log()">Save</button> -->
									<button id='updatelog' onclick="update_log()">Update</button>
									<button id='clearlog' onclick="clear_log()">Clear/Refresh</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<!-- <form id="search_form"> -->
					<form>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="contacts_div">
									<table class="display" id="tblchglog"  style="width:100%">
										<thead>
											<tr>
												<th>id</th>
												<th>version</th>
												<th>feature</th>
												<th>editor</th>
												<th>date</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>id</th>
												<th>version</th>
												<th>feature</th>
												<th>editor</th>
												<th>date</th>
											</tr>
										</tfoot>
										
									</table>
								</div>
							</div><!-- /.body -->
						</div><!-- /.panel -->
					</form>
				</div><!-- /.col-lg-12 -->
			</div><!-- /. row -->
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
	<?php print $ui->getRightSidebar($user->getUserId(), $user->getUserName(), $user->getUserAvatar()); ?>
</div><!-- ./wrapper -->

<?php print $ui->standardizedThemeJS(); ?>

<!-- CHOSEN-->
<script src="js/dashboard/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

<!-- BEGIN willson crm js -->
<script type="text/javascript" src="DataTables/Select-1.3.1/js/select.dataTables.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<!-- END willson crm js -->

<script type="text/javascript">
	$(document).ready(function() {
		inittable();
	} );// end of document.ready

	function save_log(){
		var chgid = $('#chgid').text;
		var data = {
			"api"		: 'chglog_insert',
			"version"	: $('#version').value ,
			"feature"	: $('#feature').value ,
			"editor"	: $('#editor').value ,
			"date"		: $('#date').value 
		};
		$.post("/php/apijs/apijs.php", data);
		//reload table
		$('#tblchglog').DataTable().ajax.reload();
		clear_log();
	}

	function update_log(){
		var chgid = document.getElementById("txtchgid").value;
		var version = document.getElementById("txtversion").value;
		var feature = document.getElementById("txtfeature").value;
		var editor = document.getElementById("txteditor").value;
		var date = document.getElementById("txtdate").value;

		var data = {
			"api"		: 'chglog',
			"chgid" 	: chgid ,
			"version"	: version ,
			"feature"	: feature ,
			"editor"	: editor ,
			"date"		: date 
		};

		if( chgid != ""){
			$.post("/php/apijs/apijs.php", data);
		}else if(version != "" && feature != "" && editor != ""){
			$.post("/php/apijs/apijs.php", data);
		}
		setTimeout(clear_log, 500);
		
	}
	
	function clear_log(){
		//swap save btn and update
		// $('#savelog').hide();
		// $('#updatelog').show();
			document.getElementById("txtchgid").value = "";
			document.getElementById("txtversion").value = "";
			document.getElementById("txtfeature").value = "";
			document.getElementById("txteditor").value = "";
			document.getElementById("txtdate").value = "";
		//reload table
		$('#tblchglog').DataTable().ajax.reload();
		
	}
	

	function inittable(){
		
		// Setup - add a text input to each thead cell
		$('#tblchglog thead th').each( function () {
			var title = $(this).text();
			$(this).html( title + '<input type="text"  style="width:100%" />' );
		} );

		var groupColumn = 1;
		var table = $('#tblchglog').DataTable({
			"dom"			: '<"top"l>rt<"bottom"ip><"clear">',
			"serverSide"	: true,
			"select"		: true,
			"pagingType"	: "full_numbers",
			"aLengthMenu"	: [[ 25, 50, 100], [25, 50, 100]],
			"ajax"			: "./php/server_processing/getchglog_sp.php" ,//this need change
			"columnDefs": [
				{ "visible": false, "targets": groupColumn }
			],
			"order": [[ groupColumn, 'desc' ]],
			"drawCallback": function ( settings ) {
				var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last=null;

				api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
					if ( last !== group ) {
						$(rows).eq( i ).before(
							'<tr class="group"><td colspan="5">'+group+'</td></tr>'
						);

						last = group;
					}
				} );
			},
			// search by indi col
			"initComplete"	: function () {
				// Apply the search
				this.api().columns().every( function () {
					var that = this;

					$( 'input', this.header() ).on( 'keyup change clear', function () {
						if ( that.search() !== this.value ) {
							that
								.search( this.value )
								.draw();
						}
					} );
				} );
			}
		} );

		// Order by the grouping
		$('#tblchglog tbody').on( 'click', 'tr.group', function () {
			var currentOrder = table.order()[0];
			if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
				table.order( [ groupColumn, 'desc' ] ).draw();
			}
			else {
				table.order( [ groupColumn, 'asc' ] ).draw();
			}
		} );
		
		$('#tblchglog tbody').on('click', 'tr', function () {
			//swap save btn and update
			// $('#savelog').hide();
			// $('#updatelog').show();

			var data = table.row( this ).data();
			// console.log( 'You clicked on '+data[0]+'\'s row' );
			document.getElementById("txtchgid").value = data[0];
			document.getElementById("txtversion").value = data[1];
			document.getElementById("txtfeature").value = data[2];
			document.getElementById("txteditor").value = data[3];
			document.getElementById("txtdate").value = data[4];
		} );
	}
</script>

</body>

</html>